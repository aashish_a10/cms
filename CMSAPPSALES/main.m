//
//  main.m
//  CMSAPPSALES
//
//  Created by Mac User on 30/05/12.
//  Copyright (c) 2012 vikram@in.eclatinfotech.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
