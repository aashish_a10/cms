//
//  AppDelegate.m
//  CMSAPPSALES
//
//  Created by Mac User on 30/05/12.
//  Copyright (c) 2012 vikram@in.eclatinfotech.com. All rights reserved.
//

#import "AppDelegate.h"
#import "Webservice.h"
#import "ViewController.h"
#import "GANTracker.h"
#import "TestFlight.h"

// Dispatch period in seconds
//Include the CFNetwork framework in your project and link against libsqlite3.0.dylib
static const NSInteger kGANDispatchPeriodSec = 10;
// **************************************************************************
// PLEASE REPLACE WITH YOUR ACCOUNT DETAILS.
// **************************************************************************
static NSString* const kAnalyticsAccountId = @"UA-30228148-3";

@implementation AppDelegate
@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions   
{
    //appid =@"719";
    appid =@"765";
    serverURL = @"http://appsnstuff.se/";
    //serverURL =@"http://www.myworkreview.net/";
    [TestFlight takeOff:@"176eeaa3-f678-4272-b010-05154a1fd2fc"];
    NSString  *ver = [UIDevice currentDevice].systemVersion;
    NSLog(@"%@",ver);
    application.applicationIconBadgeNumber = 0;
    ws = [[Webservice alloc]init];
    vv = [[ViewController alloc]init];
    //[self animateSplashScreen];
    NSLog(@"Initiating remoteNoticationssAreActive"); 
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound |UIRemoteNotificationTypeAlert)];     
    flag = 111;
    [[GANTracker sharedTracker] startTrackerWithAccountID:kAnalyticsAccountId
                                           dispatchPeriod:kGANDispatchPeriodSec
                                                 delegate:nil];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    } 
    else 
    {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPad" bundle:nil];
    }
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIFont fontWithName:@"Arial" size:11.0f], UITextAttributeFont,
                                                       [UIColor whiteColor], UITextAttributeTextColor,
                                                       [UIColor grayColor], UITextAttributeTextShadowColor,
                                                       [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)], UITextAttributeTextShadowOffset,
                                                       nil] forState:UIControlStateNormal];

    return YES;
}

- (void)application:(UIApplication *)application didChangeStatusBarFrame:(CGRect)oldStatusBarFrame
{
   [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackTranslucent];
}

- (void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarAnimationSlide animated:YES];
//    [vv resizeview:newStatusBarFrame];
}
/////////////////////////PUSH NOTIFICATIONS CODE /////////////////////////


- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken 
{          
    NSLog(@"devToken=%@",deviceToken);
    
    NSString *strdevicetoken11 = [[[deviceToken description]
                                   stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] 
                                  stringByReplacingOccurrencesOfString:@" " 
                                  withString:@""];
    NSString *strudid = [BPXLUUIDHandler UUID]; 
    NSLog(@"UDID %@", strudid);
    
    [ws callWebService123:[appid intValue] :strudid :strdevicetoken11:serverURL];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err 
{     
    NSLog(@"Error in registration. Error: %@", err);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo 
{
    NSLog(@"%@",userInfo);
    if ( application.applicationState == UIApplicationStateActive )
    {
        for (id key in userInfo) 
        {
            NSLog(@"key: %@, value: %@", key, [userInfo objectForKey:key]);
            dict111 = [userInfo objectForKey:key];
        }    
        [self performSelectorInBackground:@selector(getdata) withObject:nil];
    }
    else 
    {
        NSString *str = [dict111 valueForKey:@"badge"];
        application.applicationIconBadgeNumber = [str intValue];
        [self performSelectorInBackground:@selector(getdata) withObject:nil];
    }
}


-(void)getdata
{
    [vv gettabshtmldata];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        ann=333;
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        [vv gettabshtmldata];
    }
    [alertView dismissWithClickedButtonIndex:1 animated:YES];
}
////////////////////////////////////////////////////
- (void) animateSplashScreen
{
    //fade time
    CFTimeInterval animation_duration = 3.0;
    //SplashScreen 
    UIImageView * splashView;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        splashView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 320, 480)];
        splashView.image = [UIImage imageNamed:@"iphone-welcome.png"];
    }
    else
    {
        splashView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 768, 1040)];
        splashView.image = [UIImage imageNamed:@"ipadlaunch.jpeg"];
    }
    [_window addSubview:splashView];
    [_window bringSubviewToFront:splashView];
    //Animation (fade away with zoom effect)
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animation_duration];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:_window cache:YES];
    [UIView setAnimationDelegate:splashView];
    [UIView setAnimationDidStopSelector:@selector(removeFromSuperview)];
    splashView.alpha = 0.0;
    //splashView.frame = CGRectMake(-60, -60, 440, 600);
    [UIView commitAnimations];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
      [[GANTracker sharedTracker] stopTracker];
}
@end
