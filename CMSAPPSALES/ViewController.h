//
//  ViewController.h
//  CMSApp
//
//  Created by Mac User on 18/02/12.
//  Copyright (c) 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <CoreLocation/CoreLocation.h>
#import <EventKit/EventKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Webservice.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "iCarousel.h"
#import "UnderLineLabel.h"
#import "UITextFieldExt.h"
#import "BPXLUUIDHandler.h"

@class Reachability;
@class Webservice;

@class AppDelegate;
int check;
BOOL appstatus;
@interface ViewController : UIViewController<UITabBarDelegate,UIWebViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,iCarouselDataSource,iCarouselDelegate,UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate>
{
    BOOL apppassword;
    int passtag;
    NSString *strflag;
    Webservice *ws;
    UIProgressView *prog;
    NSString *currenttab;
    NSMutableArray *arraytags;
    UIActivityIndicatorView *act;
    UIView *loading;
    NSString *strUDID;
    AppDelegate *appDelegate;
    IBOutlet UITabBar *tabbar;
    UIWebView *Webview;
    UITabBarItem *tab1,*tab2,*tab3,*tab4,*tab5,*tab6,*tab7,*tab8,*tab9,*tab10; 
    NSString *str;
    NSURL* nsURL;
    CLLocationManager *locationManager;
    NSString *latitude;
    NSString *longitude;
    UITableView *table;
    CLLocationCoordinate2D coordinate;
    CLLocation *userlocation;
    NSString *strlocation;
    NSURL *urll;
    IBOutlet UINavigationBar *bar;
    IBOutlet UIButton *backitem;
    NSString *firstFive;
    UITextField *textfieldName;
    UITextField *textfieldPassword;
    int z, lastTabIndex, lastTabTypeId;
    NSMutableArray *ARRNAMES,*ARRIMAGES;
    Webservice *we;
    BOOL an;
    int flag, iTag;
    NSString *tii,*desc,*ddd, *okButtonPath;
    IBOutlet UIImageView *barimg;
    IBOutlet UILabel *lbl;
    NSString *WelcomeImagePath;
    UIView *actview;
    UILabel *lblpercent;
    int cb;
    UIView *blackview;
    NSMutableData *updateData;
    int totalTabSize;
    int currentDownloadedBytes;
    bool backButtonAdjusted;
    
    UIView *MenuView;
    UITableView *basicMenuTable;
    UIWebView *webtop;
    UIWebView *webbottom;
    UIWebView *WebTopSquare;
    UIWebView *DetailWebView;
    UIWebView *FullScreenWebView;
    
    UIWebView *cameraFormWebView;
    UIView *cameraFormFieldsView;
    
    UIWebView *confirmationMessageWebView;
    UIView *confirmationMessageView;
    
    UIImageView *IMGSEPERATOR;
    
    UITapGestureRecognizer* DoubleTap;
    
    CGFloat screenWidth, screenHeight, topBarHeight, tabBarHeight, mainBodyHeight, heightFactor;
    bool isIPhone, isIPad, isBasicMenu;
    bool isFullScreenMode, isCalledFromMenu, isCalledFromBasiciPad, useHistory, isTopLoaded, isBottomLoaded, isMainViewLoaded;
    int lastBasicTabIdiPad, doLaunchDialer, lastMenuTabIndex, detailWebViewURLHistoryCount, keyboardOffset, cameraFormHeight;
    
    UIImageView *Basicimgbackground;
    NSString *StrFullScreen, *telURL, *strUrlForWebView, *fullScreenURL;
    CGRect fullframe, detailWebViewFrame;
    UIActivityIndicatorView *loader;
    NSURLRequest *WebBottomRequest;
    UIActivityIndicatorView *acc,*ACCDETAILED;
    BOOL SquareMenuCheck;
    
    /******* Camera Form assets *****************/
    UIView *cameraFormView;
    UIImageView *camFormImageView;
    UIImage *camFormSelectedImage;
    
    IBOutlet UIToolbar *keyboardToolbar;
	UISegmentedControl *nextPreviousControl;
   /******* Camera Form assets *****************/
    
    BOOL keyboardToolbarShouldHide;

    UILabel *loadLabel;
    
}

@property (nonatomic, retain) UISegmentedControl *nextPreviousControl;
@property (nonatomic, retain) UIToolbar *keyboardToolbar;

@property (retain, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (nonatomic, retain) UIProgressView *prog;
@property (retain, nonatomic) IBOutlet UIView *SquareView;
@property (retain, nonatomic) IBOutlet UIPageControl *PageDots;
@property (nonatomic, retain) IBOutlet iCarousel *carousel1;

@property (retain, nonatomic) NSMutableArray *arrpaths;
@property (retain, nonatomic) CLLocationManager *locationManager;
@property (retain, nonatomic) CLLocation *currentLocation;
@property (retain, nonatomic) UITableView *table;
@property (retain, nonatomic) UIWebView *Webview;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (retain, nonatomic) NSMutableArray *appname,*TAB_TYPES,*arrapptabtypeid,*arrtabs,*arrtabnames,*arrtabicons,*arrtabimgs,*arrtabid,*arrapptabid,*arrtabhtml,*arrhtmlpaths,*arrurls,*arrtaburls,*arrpublisheddate,*arrdates,*arrHeaderBackgroungcolor,*arrHeaderBackgroungcoloripad,*arrheader,*arrheaderipad,*arrButtonBackgroundColor,*arrButtonBackgroundColoripad,*arrButtonBackgroungdImage,*arrHeaderBackgroungdImage,*arrButtonBackgroungdImageipad,*arrHeaderBackgroungdImageipad,*arrHeaderFontStyle,*arrbuttonFontStyle,*arrheaderfonttype,*arrHeaderTextcolor,*arrButtonTextcolor,*arrpassword,*arrpasswordstatus,*arrheaderfontsize,*arrbuttonfontsize,*arrtabsize,*arrtabhide,*arrVisibleTabs, *arrMenuTabsNavigated;

@property (nonatomic, retain)id arrmenuItems;//updated from id to ...
@property (retain, nonatomic) NSMutableDictionary *dict, *oldTabs, *nTabs, *currentTabs, *dictSquareMenuSelectedItems,*dictTabMenuItemsArray, *dictCameraForm;
@property (retain, nonatomic) NSMutableArray *arr, *arrCameraFormFields, *arrFormFieldObjects;
@property (nonatomic,assign) UIGestureRecognizerState gestureState;

- (IBAction)nextPrevious:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
- (IBAction)editingChanged:(id)sender;

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error;

- (void) getzip;

- (NSString*) getsaveddata;

- (void) jsondat:(NSString*)data;

- (void) gettabshtmldata;

- (BOOL) connectedToNetwork;

- (void) getcommondata;

- (void) checkstatus;

- (void) loaddata;

- (void) showWelcomeImage:(NSString*)welcomeImagePath;

- (UIColor *) colorForHex:(NSString *)hexColor;
- (UIColor *) colorForHex:(NSString *)hexColor : (CGFloat) opacity;

- (void) gethtml:(NSURL*)path :(NSString*)bartitle :(int)index :(int)itemTag;
- (UIImage *) resizeImage:(UIImage*)image newSize:(CGSize)newSize ;
- (void) reloadwebview :(id)sender;
- (void) addEvent;


- (void) gettabshtmldata;
- (void) showLoader;
- (void) resizeview :(CGRect)rect;
-(void) getstatus;
-(void)getappstatus;
-(void)updateProgress:(float)number;
-(NSString*) getTabFolderBasePath;
-(NSString*) getValueFromDictionary:(NSMutableDictionary *)dictObj : (NSString *)key;
-(void)processMenuItem:(int)menuItemIndex;
-(void)setSelectedTab:(int)index;
-(void)LoadBasicTab :(NSURL*)path :(int)index;
-(void)LoadTabPreview :(int)index;
-(void)LoadCameraFormTab :(NSURL*)path :(int)index;
-(void)loadFormFields:(NSMutableArray*) formFields : (NSString*) sendButtonImgPath : (NSMutableDictionary*) dictCamProperties;
-(void) PostData :(NSNumber*) index;

-(void) OnPostDataCompleted;

-(CGFloat)CheckDeviceSize;
-(void)initializeWebView;
-(void)resizeBasicView;
-(void)storeLastTabIdInArr;
-(void)resizeDetailView;
- (void)showBlackViewLoader;
- (void) resizeCameraFormImage;
- (void) moveFieldsUp:(CGRect)textFieldRect;

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (float) i_height;


@end
