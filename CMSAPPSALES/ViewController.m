//
//  ViewController.m
//  CMSApp
//
//  Created by Mac User on 18/02/12.
//  Copyright (c) 2012. All rights reserved.
//

#import "ViewController.h"
#import "SBJson.h"
#import "SSZipArchive.h"
#import "Reachability.h"
#import "Webservice.h"
#import "GANTracker.h"
#import "RequestQueue.h"
#import "UIView+firstResponder.h"
#import "TestFlight.h"


@implementation ViewController

@synthesize prog;
@synthesize carousel1;
@synthesize arrpaths;
@synthesize locationManager, currentLocation;
@synthesize table;
@synthesize Webview;
@synthesize coordinate;
@synthesize appname,TAB_TYPES,arrapptabtypeid,arrtabs,arrtabnames,arrtabicons,arrtabimgs,arrtabid,arrapptabid,arrtabhtml,arrhtmlpaths,arrurls,arrtaburls,arrpublisheddate,arrdates,arrHeaderBackgroungcolor,arrButtonBackgroundColoripad,arrheader,arrButtonBackgroundColor,arrHeaderBackgroungcoloripad,arrButtonBackgroungdImage,arrButtonBackgroungdImageipad,arrHeaderBackgroungdImage,arrHeaderBackgroungdImageipad,arrHeaderFontStyle,arrbuttonFontStyle,arrHeaderTextcolor,arrButtonTextcolor,arrpassword,arrpasswordstatus,arrheaderfontsize,arrbuttonfontsize,arrheaderfonttype,arrheaderipad,arrtabsize,arrtabhide,arrmenuItems,arrVisibleTabs,arrMenuTabsNavigated,arrCameraFormFields, arrFormFieldObjects;
@synthesize dict, oldTabs, nTabs, currentTabs, dictSquareMenuSelectedItems, dictTabMenuItemsArray, dictCameraForm;
@synthesize arr;
@synthesize gestureState;

@synthesize nextPreviousControl;
@synthesize keyboardToolbar;


CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.3;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 250;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (NSString*) getTabFolderBasePath
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath= [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    
    return dataPath;
}

- (NSString*) getValueFromDictionary:(NSMutableDictionary *)dictObj : (NSString *)key;
{
    NSString *val = [dictObj valueForKey:key];
    
    if(val == (NSString *)[NSNull null])
    {
        val = @"";
    }
    
    return val;
}

- (void)showalert
{
    UIAlertView * alert = [[[UIAlertView alloc] initWithTitle:@"Password Protected" message:@"Enter Username and Password:" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue",nil]autorelease];
    alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    textfieldName =[alert textFieldAtIndex:0];
    textfieldPassword = [alert textFieldAtIndex:1];
    textfieldName.keyboardType = UIKeyboardTypeDefault;
    textfieldName.placeholder = @"Enter Username";
    textfieldPassword.keyboardType = UIKeyboardTypeDefault;
    textfieldPassword.placeholder = @"Enter Password";
    
    /*
     if (flag == 1)
    {
        NSString *strtitle = [arrtabnames objectAtIndex:z];
        str   =[arrhtmlpaths objectAtIndex:z];  
        nsURL = [ NSURL fileURLWithPath :str];
        [self gethtml:nsURL :strtitle :z];
    }
    else 
     */
    {
        [alert show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(doLaunchDialer)
    {
        doLaunchDialer = false;
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:@"Ring"])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telURL]];
        }
    }
    else
    {
        // Clicked the Submit button
        if (apppassword != YES)
        {
            
            if (buttonIndex != [alertView cancelButtonIndex])
            {
                NSLog(@"Name: %@", textfieldName.text);
                NSLog(@"Name: %@", textfieldPassword.text);//[
                [we callWebService:[appid intValue] :[[arrapptabid objectAtIndex:z] intValue] :textfieldName.text :textfieldPassword.text:serverURL];
                NSLog(@"%@",resu);
                NSLog(@"%@",[we callWebService:[appid intValue] :[[arrapptabid objectAtIndex:z] intValue] :textfieldName.text :textfieldPassword.text:serverURL]);
                [self performSelector:@selector(getstatus) withObject:nil afterDelay:1];
                NSLog(@"resuresuresu%@",resu);
                if (passtag==212)
                {
                    [tabbar setSelectedItem:tab1];
                    passtag = 200;
                }
                else
                {
                    if (z >= 4)
                    {
                        [tabbar setSelectedItem:[tabbar.items objectAtIndex:4]];
                    }
                    else
                    {
                        [tabbar setSelectedItem:[tabbar.items objectAtIndex:z]];
                    }
                }
            }
            else
            {
                if (z >= 4)
                {
                    [arraytags removeLastObject];
                    NSNumber *ptag = [arraytags lastObject];
                    int ptagIndex = -1;
                    for (int cnter = 0 ; cnter < arrVisibleTabs.count; cnter++) {
                        if([arrVisibleTabs objectAtIndex:cnter] == ptag)
                        {
                            ptagIndex = cnter;
                            break;
                        }
                    }
                    if(ptagIndex > -1)
                    {
                        if (ptagIndex >= 4)
                        {
                            [tabbar setSelectedItem:[tabbar.items objectAtIndex:4]];
                        }
                        else
                        {
                            [tabbar setSelectedItem:[tabbar.items objectAtIndex:ptagIndex ]];
                        }
                    }
                    [arraytags removeAllObjects];

                    
                    //[tabbar setSelectedItem:[tabbar.items objectAtIndex:3]];
                }
                else
                {
                    [arraytags removeLastObject];
                    NSNumber *ptag = [arraytags lastObject];
                    int ptagIndex = -1;
                    for (int cnter = 0 ; cnter < arrVisibleTabs.count; cnter++) {
                        if([arrVisibleTabs objectAtIndex:cnter] == ptag)
                        {
                            ptagIndex = cnter;
                            break;
                        }
                    }
                    if(ptagIndex > -1)
                    {
                        if (ptagIndex >=4)
                        {
                            [tabbar setSelectedItem:[tabbar.items objectAtIndex:4]];
                        }
                        else
                        {
                            [tabbar setSelectedItem:[tabbar.items objectAtIndex:ptagIndex ]];
                        }
                    }
                    [arraytags removeAllObjects];
                }

            }
        }
        else
        {
            if (buttonIndex != [alertView cancelButtonIndex])
            {
                NSLog(@"Name: %@", textfieldName.text);
                NSLog(@"Name: %@", textfieldPassword.text);//[
                [ws callWebServiceapp:[appid intValue] :[appid intValue] :textfieldName.text :textfieldPassword.text :serverURL];
                NSLog(@"%@",[we callWebServiceapp:[appid intValue] :[appid intValue] :textfieldName.text :textfieldPassword.text :serverURL]);
                [self performSelector:@selector(getappstatus) withObject:nil afterDelay:1];
                NSLog(@"resuresuresu%@",resu);
            }
            else
            {
                
            }
            
        }
    }
}

- (void) getstatus
{
    NSString *strtitle = [arrtabnames objectAtIndex:z];
    if ([resu isEqualToString:@"true"]) 
    {
        [arrpassword replaceObjectAtIndex:z withObject:@"0"];
        str   =[arrhtmlpaths objectAtIndex:z];  
        nsURL = [ NSURL fileURLWithPath :str];
        [self gethtml:nsURL :strtitle :z:iTag];
        flag = 1;
        [table removeFromSuperview];
        if (z > 3)
        {
            [tabbar setSelectedItem:tab6];
        }
        else
        {
            [tabbar setSelectedItem:[tabbar.items objectAtIndex:z]];
        }
    } 
    else 
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"Incorrect Credentials" message:@"Enter Valid Userid and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
        [alt release];
        if(table != nil)
        {
            [table removeFromSuperview];
        }
    }
}

-(void)getappstatus
{
    if ([resu isEqualToString:@"true"])
    {
        if(!arrtabhtml)
        {
        arrtabhtml =[[NSMutableArray alloc]init];
        }
        if(!arrhtmlpaths)
        {
        arrhtmlpaths =[[NSMutableArray alloc]init];
        }
        if(!arrpublisheddate)
        {
        arrpublisheddate =[[NSMutableArray alloc]init];
        }
        if(!arrdates)
        {
        arrdates =[[NSMutableArray alloc]init];
        }
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
            [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
        
        for (int i=0; i<[arrapptabid count]; i++)
        {
            long oldTick = 0;
            if ([oldTabs objectForKey:[arrapptabid objectAtIndex:i]] != nil )
            {
                oldTick = [[oldTabs objectForKey:[arrapptabid objectAtIndex:i]] timeIntervalSince1970];
            }
            long newTick = 0;
            if ([currentTabs objectForKey:[arrapptabid objectAtIndex:i]] != nil )
            {
                newTick = [[currentTabs objectForKey:[arrapptabid objectAtIndex:i]] timeIntervalSince1970];
            }
            //Check if currentTabId exists in older tab data
            if (newTick > oldTick )
            {
                [self downloadDataForTab:appid :[arrtabid objectAtIndex:i]:i];
            }
            
            
        }
        for (int i=0; i<[arrapptabid count]; i++)
        {
            NSString *tabdataPath;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@",appid,[arrapptabid objectAtIndex:i]]];
            }
            else
            {
                tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@",appid,[arrapptabid objectAtIndex:i]]];
            }
            
            NSString *tabhtmlpath = [tabdataPath stringByAppendingPathComponent:@"index.html"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:tabhtmlpath])
            {
                [arrtabsize replaceObjectAtIndex:i withObject:[arrtabsize objectAtIndex:i]];
            }
            else
            {
                [arrtabsize replaceObjectAtIndex:i withObject:@"0"];
            }
            
        }
        
        
        for (int i=0; i<[arrapptabid count]; i++)
        {
            NSString *tabdataPath;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@",appid,[arrapptabid objectAtIndex:i]]];
            }
            else
            {
                tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@",appid,[arrapptabid objectAtIndex:i]]];
            }
            
            NSString *tabhtmlpath = [tabdataPath stringByAppendingPathComponent:@"index.html"];
            if (![[NSFileManager defaultManager] fileExistsAtPath:tabhtmlpath])
            {
                [self downloadDataForTab:appid :[arrtabid objectAtIndex:i]:i];
                
                [arrhtmlpaths addObject:tabhtmlpath];
            }
            else
            {
                [arrhtmlpaths addObject:tabhtmlpath];
            }
        }
        apppassword = NO;
        [self getcommondata];
        
        
    }
    else
    {
        [we callWebService:[appid intValue] :[[arrapptabid objectAtIndex:z] intValue] :textfieldName.text :textfieldPassword.text:serverURL];
        UIAlertView *alt = [[[UIAlertView alloc]initWithTitle:@"Incorrect Credentials" message:@"Enter Valid Userid and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]autorelease];
        [alt show];
        
        [table removeFromSuperview];
        [self showalert];
        //[alt release];
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [TestFlight passCheckpoint:@"Tab Selected."]; 
    isCalledFromMenu = false;
    isCalledFromBasiciPad = false;
    
    [dictSquareMenuSelectedItems removeAllObjects];
    
    lastTabTypeId = 0;
    if (item == tab1) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:0]intValue] :item.tag];
    }
    else if (item == tab2) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:1]intValue] :item.tag];
    }
    else if (item == tab3) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:2]intValue] :item.tag];
    }
    else if (item == tab4)
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:3]intValue] :item.tag];
    }
    else if (item == tab5) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:4]intValue] :item.tag];
    }
    else if (item == tab7) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:5]intValue] :item.tag];
    }
    else if (item == tab8) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:6]intValue] :item.tag];
    }
    else if (item == tab9) 
    {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:7]intValue] :item.tag];
    }
    else if (item == tab10)
    {
        [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:8]intValue] :item.tag];
    }
    else if (item == tab6)
    { // Handling of more button.
         
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            float heightFactor1 = 0;
            screenHeight = [ [ UIScreen mainScreen ] bounds ].size.height;
            if( screenHeight > 480.0){
                heightFactor1 = screenHeight - 480;
            }
            if(!table)
            table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 360, 411+ heightFactor1) style:UITableViewStylePlain];
            
        }
        else
        {
            if(!table)
            table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 1024, 698) style:UITableViewStylePlain];
            table.autoresizesSubviews = YES;
        }
        [table setDelegate:self];
        [table setDataSource:self];
        [self.view addSubview:table];  
        [self.view bringSubviewToFront:tabbar];
        [table reloadData];
    }
}

-(void)storeLastTabIdInArr
{
    int icount = [arrMenuTabsNavigated count]-1;
    bool addLastTabIndex = true;
    if(icount >= 0)
    {
        if([[arrMenuTabsNavigated objectAtIndex:icount] intValue ] == lastMenuTabIndex )
        {
            addLastTabIndex = false;
        }
    }
    
    if(addLastTabIndex)
    {
        [arrMenuTabsNavigated addObject:[NSNumber numberWithInt:lastMenuTabIndex]];
    }
}

-(void)createURLAndLoad:(int)index :(int)itemTag
{
    [locationManager stopUpdatingLocation];
    iTag = itemTag;
    tabbar.hidden = false;
    [self.view bringSubviewToFront:tabbar];
    int ttypeId =0;
    if (itemTag ==111 || itemTag == 555)
    {
        lastTabIndex = z;
        lastTabTypeId = [[arrapptabtypeid objectAtIndex:z] intValue];
        ttypeId = [[arrapptabtypeid objectAtIndex:index] intValue];
        if(isIPad && (lastTabTypeId == 12 || (ttypeId == 12 || ttypeId == 13)) )
        {
            if(ttypeId ==12 || ttypeId ==13)
            {
                [self storeLastTabIdInArr];
            }
        }
        else if(lastTabTypeId == 12 || lastTabTypeId == 13)
            //else if(ttypeId == 12 || ttypeId == 13)
        {
            [self storeLastTabIdInArr];
        }    }
    strUDID = [BPXLUUIDHandler UUID];
    z= index;
    ttypeId = [[arrapptabtypeid objectAtIndex:z] intValue];
    if(ttypeId ==12 || ttypeId ==13)
    {
        lastMenuTabIndex = z;
    }
    
    if(itemTag != 555 )
    {
        [self setSelectedTab:z];
    }
    NSNumber *nnn = [NSNumber numberWithInt:z];
    [arraytags addObject:nnn];
    NSString *strTitle = [arrtabnames objectAtIndex:index];
    [table removeFromSuperview];  
    if ([[arrtaburls objectAtIndex:index] isEqualToString:@""])
    {
        NSLog(@"arrpassword %@",[arrpassword objectAtIndex:index]);
        if ([[arrpassword objectAtIndex:index] boolValue] ==an)
        {
            passtag = 212;
            [self showalert];   
        }
        else
        {
            if ([[arrapptabtypeid objectAtIndex:index] isEqualToString:@"2"])
            {
                if ([[UIDevice currentDevice].systemVersion floatValue]<5.0) 
                {
                    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                    NSString *dataPath= [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
                    NSString *tabdataPath;
                    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                    {
                        tabdataPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@",appid,[arrapptabid objectAtIndex:index]]];
                    }
                    else 
                    {
                        tabdataPath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@",appid,[arrapptabid objectAtIndex:index]]];
                    }
                    NSString *tabhtmlpath = [tabdataPath stringByAppendingPathComponent:@"index4.html"];
                    str   =tabhtmlpath; 
                }
                else 
                {
                    str   =[arrhtmlpaths objectAtIndex:index]; 
                }
                nsURL = [ NSURL fileURLWithPath :str];
                fullScreenURL = str;
                str = [nsURL absoluteString];
                str   =[str stringByAppendingFormat:[NSString stringWithFormat:@"?IMEI=%@",strUDID]];
                nsURL =[NSURL URLWithString:str];
                [self gethtml:nsURL :strTitle :index:itemTag];
            }
            else 
            {
            str   =[arrhtmlpaths objectAtIndex:index];
            fullScreenURL = str;
            nsURL = [ NSURL fileURLWithPath :str];
            str = [nsURL absoluteString];
            str   =[str stringByAppendingFormat:[NSString stringWithFormat:@"?IMEI=%@",strUDID]];
            nsURL =[NSURL URLWithString:str];
                [self gethtml:nsURL :strTitle :index:itemTag];
            }
        }
    } 
    else 
    {
        check = 999;
        //[self showLoader];
        int r = rand();
        strlocation = @""; //Setting location as empty as reverse geocoding being handled in map code itself.
        NSString *stru =  [arrtaburls objectAtIndex:index];
        NSString *strurl = [stru stringByAppendingFormat:[NSString stringWithFormat:@"AppTabID=%@&cLocation=%@&cLatitude=%@&cLongitude=%@&IMEI=%@&guid=%i",[arrtabid objectAtIndex:index],strlocation,latitude,longitude,strUDID,r]];
        nsURL =[NSURL URLWithString:[strurl stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        fullScreenURL = [nsURL absoluteString];
        [self gethtml:nsURL :strTitle :index:itemTag];
    }    
}

- (void)resizeview :(CGRect)rect
{
    table.frame = CGRectMake(rect.size.height, rect.origin.y, table.frame.size.width,(table.frame.size.height - rect.size.height));
    Webview.frame = CGRectMake(rect.size.height, rect.origin.y, Webview.frame.size.width,(Webview.frame.size.height - rect.size.height));
}

-(void)gethtml:(NSURL*)path :(NSString*)bartitle :(int)index:(int)itemTag
{
    useHistory = true;
    int ttypeId = [[arrapptabtypeid objectAtIndex:index] intValue];
    if(itemTag == 555 && ttypeId !=12 && ttypeId !=13 && !isCalledFromBasiciPad) //Load tab preview in basic menu iPad, provided tab is not a menu tab (12 or 13)
    {

        detailWebViewURLHistoryCount = 0;
        NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        [DetailWebView loadRequest:request];
    }
    else
    {
        @try
        {
            [self initializeWebView];
            barimg.image = nil;
            backitem.imageView.image = nil;
            lbl.textAlignment =UITextAlignmentCenter ;
            [self.table removeFromSuperview];
            self.table = nil;
            currenttab = [TAB_TYPES objectAtIndex:[[arrapptabtypeid objectAtIndex:index] intValue]];
            
            NSError *error;
            if (![[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"%@",[TAB_TYPES objectAtIndex:[[arrapptabtypeid objectAtIndex:index] intValue]]]withError:&error])
            {
                NSLog(@"error in trackPageview%@",[error description]);
            }
            lbl.font = [UIFont fontWithName:[arrHeaderFontStyle objectAtIndex:index] size:[[arrheaderfontsize objectAtIndex:index] intValue]];
            lbl.text =bartitle;
            lbl.backgroundColor = [UIColor clearColor];
            lbl.textColor = [self colorForHex:[arrHeaderTextcolor objectAtIndex:index]];
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *dataPath= [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
            NSString *imgpath;
            NSString *ImagedataPath = [dataPath stringByAppendingPathComponent:@"Images"];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[arrHeaderBackgroungdImage objectAtIndex:index]]];
            }
            else
            {
                imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[arrHeaderBackgroungdImageipad objectAtIndex:index]]];
            }
            NSLog(@"Header bkg imgPath: %@,  ImageDataPath: %@",imgpath,ImagedataPath);
            if ([imgpath isEqualToString:ImagedataPath])
            {
                UIColor *coll = [self colorForHex:[arrHeaderBackgroungcolor objectAtIndex:index]];
                NSLog(@"Header bkg color: %@",[arrHeaderBackgroungcolor objectAtIndex:index]);
                [barimg setBackgroundColor:coll];
            }
            else
            {
                barimg.image = [UIImage imageWithContentsOfFile:imgpath];
            }
            
            NSString *buttonimgpath;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                buttonimgpath  = [ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[arrButtonBackgroungdImage objectAtIndex:index]]];
            }
            else
            {
                buttonimgpath  = [ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[arrButtonBackgroungdImageipad objectAtIndex:index]]];
            }
            
            NSLog(@"back button imgPath: %@,  ImageDataPath: %@",buttonimgpath,[arrButtonBackgroungdImageipad objectAtIndex:index]);
            if ([buttonimgpath isEqualToString:ImagedataPath])
            {
                [backitem setImage:nil forState:UIControlStateNormal];
                [backitem setBackgroundColor:[self colorForHex:[arrButtonBackgroundColor objectAtIndex:index]]];
                NSLog(@"back btton bkg color: %@",[arrButtonBackgroundColor objectAtIndex:index]);
                [backitem setTitle:@"Back" forState:UIControlStateNormal];
                backitem.titleLabel.font = [UIFont fontWithName:[arrbuttonFontStyle objectAtIndex:index] size:[[arrbuttonfontsize objectAtIndex:index]intValue]];
                [backitem setTitleColor:[self colorForHex:[arrButtonTextcolor objectAtIndex:index]] forState:UIControlStateNormal];
            }
            else
            {
                UIImage *backButtonImage = [UIImage imageWithContentsOfFile:buttonimgpath];
                if ([UIScreen mainScreen].scale != 2.0)
                {
                    CGSize size = backButtonImage.size;
                    NSLog(@"%f  %f",size.height,size.width);
                    backButtonImage = [self resizeImage:backButtonImage newSize:CGSizeMake(size.width/1,size.height/1)];
                    NSLog(@"non-Retina display");
                }
                [backitem setBackgroundColor:[UIColor clearColor]];
                [backitem setImage:backButtonImage forState:UIControlStateNormal];
            }
            [barimg addSubview:lbl];
            [self.view addSubview:barimg];
            [self.view bringSubviewToFront:backitem];
            [self.view bringSubviewToFront:tabbar];
            backitem.hidden = YES;
            
            if(isCalledFromBasiciPad)
            {
                [self.view bringSubviewToFront:Webview];
                [self.view bringSubviewToFront:tabbar];
            }
            
            
            //Check if tabtypeId=12 or 13 i.e., Menu, then parse JSON
            if([[arrapptabtypeid objectAtIndex:index] isEqual:@"12"] )
            {
                
                [backitem setHidden:YES];
                backitem.enabled = NO;
                
                [self LoadBasicTab:path :index];
            }
            else if ([[arrapptabtypeid objectAtIndex:index] isEqual:@"13"])
            {
                
                [backitem setHidden:YES];
                backitem.enabled = NO;
                
                [self LoadSquareMenu:path :index];
            }
            else if ([[arrapptabtypeid objectAtIndex:index] isEqual:@"14"])
            {
                
                [backitem setHidden:YES];
                backitem.enabled = NO;
                
                [self LoadCameraFormTab:path :index];
            }
            else
            {
                bool isRSS = false;
                //Check if tabtypeId=5 i.e., RSS, then download latest content
                if([[arrapptabtypeid objectAtIndex:index] isEqual:@"5"])
                {
                    isRSS = true;
                    CGRect  rect = [[UIScreen mainScreen] bounds];
                    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                    {
                        if(!actview)
                        actview = [[UIView alloc] initWithFrame:CGRectMake(0  , 0 , rect.size.width, rect.size.height)];
                    }
                    else
                    {
                         if(!actview)
                        actview = [[UIView alloc] initWithFrame:CGRectMake(0  , 0 , 1024, 768)];
                    }
                    
                    actview.backgroundColor =[UIColor whiteColor];
                    actview.opaque = NO;
                    if(!acc)
                    {
                     acc = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    }
                    if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
                    {
                        acc.color = [UIColor blackColor];
                    }
                    
                    acc.center = self.Webview.center;
                    [acc startAnimating];
                    [actview addSubview:acc];
                    UILabel *updatinglable = [[[UILabel alloc] initWithFrame:CGRectMake(rect.size.width/2 - 85 , rect.size.height/2 + 12, 200, 25)]autorelease];
                    updatinglable.text = @"Updating RSS ...";
                    updatinglable.font = [UIFont boldSystemFontOfSize:15.0f];
                    updatinglable.textAlignment = UITextAlignmentCenter;
                    updatinglable.textColor = [UIColor blackColor];
                    updatinglable.backgroundColor = [UIColor clearColor];
                    updatinglable.center = self.Webview.center;
                    [updatinglable setFrame: CGRectMake(self.Webview.center.x-90, self.Webview.center.y + 20,200,25)];
                    [actview addSubview:updatinglable];
                    [Webview addSubview:actview];
                    //[self.view bringSubviewToFront:Webview];
                    NSNumber *num = [NSNumber numberWithInt:index];
                    [self performSelectorInBackground:@selector(getrss:) withObject:num];
                    
                }
                NSString *dataPath1 = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
                NSString *tabdataPath;
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                {
                    tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@",appid,[arrapptabid objectAtIndex:index]]];
                }
                else
                {
                    tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@",appid,[arrapptabid objectAtIndex:index]]];
                }
                
                bool isFileExisting = true;
                if (![[NSFileManager defaultManager] fileExistsAtPath:tabdataPath] && isRSS == false )
                {
                    isFileExisting = false;
                    CGRect  rect = [[UIScreen mainScreen] bounds];
                    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                    {
                        if(!actview)
                        actview = [[UIView alloc] initWithFrame:CGRectMake(0  , 0 , rect.size.width, rect.size.height)];
                    }
                    else
                    {
                        if(!actview)
                        actview = [[UIView alloc] initWithFrame:CGRectMake(0  , 0 , rect.size.width, rect.size.height)];
                    }
                    
                    actview.backgroundColor = [UIColor whiteColor];
                    actview.opaque = NO;
                    
                    UIActivityIndicatorView *acc = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]autorelease];
                    
                    if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
                    {
                        acc.color = [UIColor blackColor];
                    }
                    acc.center = self.Webview.center;
                    [acc startAnimating];
                    [actview addSubview:acc];
                    UILabel *updatinglable = [[[UILabel alloc] initWithFrame:CGRectMake(rect.size.width/2 - 85 , rect.size.height/2 + 12, 200, 25)]autorelease];
                    updatinglable.text = @"Downloading Data ...";
                    updatinglable.font = [UIFont boldSystemFontOfSize:15.0f];
                    updatinglable.textAlignment = UITextAlignmentCenter;
                    updatinglable.textColor = [UIColor blackColor];
                    updatinglable.backgroundColor = [UIColor clearColor];
                    [actview addSubview:updatinglable];
                    [Webview addSubview:actview];
                    
                    NSNumber *num = [NSNumber numberWithInt:index];
                    
                    [self performSelectorInBackground:@selector(getlostdata:) withObject:num];
                }
                if(isFileExisting == true || isRSS == true)
                {
                    
                    NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                              cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval: 1000];
                    [Webview loadRequest:request];
                    check = 0;
                }
                strUrlForWebView = @"";
            }
        }
        @catch (NSException *exception) 
        {
            
        }
        
    }
}


-(void)LoadBasicTab :(NSURL*)path :(int)index
{
    isCalledFromBasiciPad = false;
    lastBasicTabIdiPad = index;
    
    isBasicMenu = true;
    
    backitem.hidden = YES;
    
    isTopLoaded = false;
    isBottomLoaded = false;
    
    webtop= nil;
    webbottom = nil;
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath1 = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSString *tabdataPath;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/%@_2_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    else
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/%@_3_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    
    bool isContentAvailable = true;
//    if(!arrmenuItems)
//    {
//        arrmenuItems = [[NSMutableArray alloc]init];
//    }
    arrmenuItems = [dictTabMenuItemsArray valueForKey:[NSString stringWithFormat:@"%d",index ]];
    
    if(arrmenuItems == nil)
    {
        isContentAvailable = false;
    }
    NSString* content;
    
    if(!isContentAvailable)
    {
       content  = [NSString stringWithContentsOfFile:tabdataPath encoding:NSUTF8StringEncoding error:NULL];
    }
    //NSLog(@"JSON Path %@ \n %@",tabdataPath, content);
    if (isContentAvailable || content != nil)
    {
        if(!isContentAvailable)
        {
            arrmenuItems = [content JSONValue];
            [dictTabMenuItemsArray setValue:arrmenuItems forKey:[NSString stringWithFormat:@"%d",index ]];
        }
        NSLog(@"Menu item count %d, first menu %@",[arrmenuItems count], [[arrmenuItems objectAtIndex:1]valueForKey:@"MenuItemName"]);
        NSMutableDictionary *menuItemDict = [arrmenuItems objectAtIndex:0];
        NSString* menuItemBgColor = [self getValueFromDictionary:menuItemDict:@"BackGroundColor"];
        NSString* menuItemBgIphoneImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIphoneImage"];
        NSString* menuItemBgIpadImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIpadImage"];
        BOOL menutabTopbar =[[self getValueFromDictionary:menuItemDict:@"MenuTopBar"] boolValue];
        
        if(!MenuView)
        { MenuView = [[UIView alloc]init]; }
        
        if(!_ScrollView)
        {
            _ScrollView = [[UIScrollView alloc]init];
        }
        
        if (menutabTopbar == NO && (!isCalledFromMenu))
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                CGFloat size = [self CheckDeviceSize];
                MenuView.frame = CGRectMake(0, 0, 320, 414+size);
                _ScrollView.frame = CGRectMake(0, 0, 320, 410+size);
                if(!basicMenuTable)
                {
                basicMenuTable = [[UITableView alloc]initWithFrame:CGRectMake(5, 44, 310,325+size) style:UITableViewStyleGrouped];
                }
                if(!webtop)
                {
                webtop = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
                }
                if(!webbottom)
                {
                webbottom = [[UIWebView alloc]initWithFrame:CGRectMake(0, 390, 320, 40)];
                }
            }
            else
            {
                MenuView.frame = CGRectMake(0, 0, 1024,700);
                _ScrollView.frame = CGRectMake(0, 0, 320,698);
                if(!basicMenuTable)
                {
                basicMenuTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, 320,698) style:UITableViewStyleGrouped];
                }
                if(!webtop)
                {
                webtop = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
                }
                if(!DetailWebView)
                {
                DetailWebView = [[UIWebView alloc]initWithFrame:CGRectMake(322, 0, 1024-322, 599+80)];
                }
                if(!IMGSEPERATOR)
                {
                IMGSEPERATOR = [[UIImageView alloc]initWithFrame:CGRectMake(320, 0, 2, 735)];
                }
                IMGSEPERATOR.backgroundColor = [UIColor grayColor];
                IMGSEPERATOR.hidden = false;
            }

            if ([[UIDevice currentDevice].systemVersion intValue] >5.1) {
                basicMenuTable.autoresizesSubviews = YES;
                
                UIView* bview = [[[UIView alloc] init]autorelease];
                bview.backgroundColor = [UIColor clearColor];
                [basicMenuTable setBackgroundView:bview];
            }
            //else
            {
                basicMenuTable.backgroundColor = [UIColor clearColor];
                basicMenuTable.opaque = NO;
                _ScrollView.backgroundColor = [UIColor clearColor];
            }
            
            if ( (isIPhone && [menuItemBgIphoneImage isEqualToString:@""]) || (isIPad && [menuItemBgIpadImage isEqualToString:@""]) )
            {
                MenuView.backgroundColor = [self colorForHex:menuItemBgColor];
                _ScrollView.backgroundColor = [UIColor clearColor];
                
            }
            else
            {
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                {
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIphoneImage]];
                    if(!Basicimgbackground)
                    {
                    Basicimgbackground = [[UIImageView alloc] initWithImage:[[UIImage imageWithContentsOfFile:imgPath]stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0]];
                    }
                    [MenuView addSubview:Basicimgbackground];
                    
                }
                else
                {
                    isFullScreenMode = false;
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIpadImage]];
                    UIImageView *imgbackground = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgPath]]autorelease];
                    imgbackground.frame = CGRectMake(0, 0, 1024,700);
                    
                    [MenuView addSubview:imgbackground];
                }
                
            }
            NSString *taPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/indexbottom.html",appid,[arrapptabid objectAtIndex:index]]];
            // = [[UIWebView alloc]initWithFrame:CGRectMake(0, 325, 320, 40)];
            webtop.opaque = NO;
            webtop.backgroundColor = [UIColor clearColor];
            webtop.delegate = self;
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                [_ScrollView addSubview:webtop];
            }
            else
            {
                
            }
            NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                      cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                  timeoutInterval: 1000];
            [webtop loadRequest:request];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                webbottom.opaque = NO;
                webbottom.backgroundColor = [UIColor clearColor];
                webbottom.delegate = self;
                [_ScrollView addSubview:webbottom];
                
                NSURLRequest * request1 = [NSURLRequest requestWithURL: [NSURL fileURLWithPath:taPath]
                                                           cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval: 1000];
                if (!WebBottomRequest) {
                    WebBottomRequest = [[NSURLRequest alloc] init];
                }
                
                WebBottomRequest = request1;
            }
            basicMenuTable.delegate = self;
            basicMenuTable.dataSource = self;
            DetailWebView.opaque = NO;
            DetailWebView.backgroundColor = [UIColor clearColor];
            DetailWebView.delegate = self;
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
               
                basicMenuTable.scrollEnabled = false;
                webtop.scrollView.scrollEnabled = false;
                webbottom.scrollView.scrollEnabled = false;
                
                [_SquareView removeFromSuperview];
                [table removeFromSuperview];
                [Webview removeFromSuperview];
                [self.view addSubview:MenuView];
                [_ScrollView addSubview:basicMenuTable];
                [self.view addSubview:_ScrollView];
                _ScrollView.scrollEnabled = true;
            }
            else
            {
                [_SquareView removeFromSuperview];
                [table removeFromSuperview];
                [Webview removeFromSuperview];
                basicMenuTable.scrollEnabled = false;
                [_ScrollView addSubview:webtop];
                [_ScrollView addSubview:basicMenuTable];
                [MenuView addSubview:_ScrollView];
                [MenuView addSubview:DetailWebView];
                [MenuView addSubview:IMGSEPERATOR];
                _ScrollView.contentSize = CGSizeMake(320, basicMenuTable.contentSize.height);
                [self.view addSubview:MenuView];
                
            }
            
            
            
        }
        else
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                CGFloat size = [self CheckDeviceSize];
                MenuView.frame = CGRectMake(0, 44, 320, 370+size);
                
                _ScrollView.frame = CGRectMake(0, 44, 320, 366+size);
                if(!basicMenuTable)
                {
                basicMenuTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, 320,275+size) style:UITableViewStyleGrouped];
                }
                if(!webtop)
                {
                    webtop = [[UIWebView alloc]initWithFrame:CGRectMake(0,0, 320, 44)];
                }
                if(!webbottom)
                {
                webbottom = [[UIWebView alloc]initWithFrame:CGRectMake(0, 320, 320, 44)];
                }
            }
            else
            {
                MenuView.frame = CGRectMake(0, 44, 1024,655);
                
                _ScrollView.frame = CGRectMake(0, 0, 320,655) ;
                if(!basicMenuTable)
                {
                basicMenuTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 80, 320,576) style:UITableViewStyleGrouped];
                }
                if(!webtop)
                {
                webtop= [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 320, 80)];
                }
                if(!DetailWebView)
                {
                DetailWebView = [[UIWebView alloc]initWithFrame:CGRectMake(322, 0, 1024-322, 655)];
                }
                StrFullScreen = @"NO";
                if(!IMGSEPERATOR)
                {
                IMGSEPERATOR = [[UIImageView alloc]initWithFrame:CGRectMake(320, 44, 2, 612)];
                }
                IMGSEPERATOR.backgroundColor = [UIColor grayColor];
                IMGSEPERATOR.hidden = false;
            }
            
            NSLog(@"%f",[[UIDevice currentDevice].systemVersion floatValue]);
            if ([[UIDevice currentDevice].systemVersion floatValue] >5.10f) {
                basicMenuTable.autoresizesSubviews = YES;
                UIView* bview = [[[UIView alloc] init]autorelease];
                bview.backgroundColor = [UIColor clearColor];
                [basicMenuTable setBackgroundView:bview];
            }
            //else
            {
                basicMenuTable.backgroundColor = [UIColor clearColor];
                basicMenuTable.opaque = NO;
                _ScrollView.backgroundColor = [UIColor clearColor];
            }
            
            if ( (isIPhone && [menuItemBgIphoneImage isEqualToString:@""]) || (isIPad && [menuItemBgIpadImage isEqualToString:@""]) )
            {
                MenuView.backgroundColor = [self colorForHex:menuItemBgColor];
                _ScrollView.backgroundColor = [UIColor clearColor];
            }
            else
            {
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                {
                    CGFloat size = [self CheckDeviceSize];
                    
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIphoneImage]];
                    UIImageView *imgbackground = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgPath]]autorelease];
                    imgbackground.frame = CGRectMake(0, 0, 320, 366+size);
                    [MenuView addSubview:imgbackground];
                    
                }
                else
                {
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIpadImage]];
                    UIImageView *imgbackground = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgPath]]autorelease];
                    imgbackground.frame = CGRectMake(0, 0, 1024,655);
                    
                    [MenuView addSubview:imgbackground];
                }
                
            }
            NSString *taPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/indexBottom.html",appid,[arrapptabid objectAtIndex:index]]];
            webtop.opaque = NO;
            webtop.backgroundColor = [UIColor clearColor];
            webtop.delegate = self;
            
            NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                      cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                  timeoutInterval: 1000];
            [webtop loadRequest:request];
            NSURLRequest * request1 = [[NSURLRequest alloc] initWithURL: [NSURL fileURLWithPath:taPath]
                                                       cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                   timeoutInterval: 1000];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                [_ScrollView addSubview:webtop];
                
                webbottom.opaque = NO;
                webbottom.backgroundColor = [UIColor clearColor];
                webbottom.delegate = self;
                [_ScrollView addSubview:webbottom];
                WebBottomRequest = request1;
               // [webbottom loadRequest:request1];   // update
            }
            else
            {
             
            }
            
            DetailWebView.opaque = NO;
            DetailWebView.backgroundColor = [UIColor clearColor];
            DetailWebView.delegate = self;
            
            basicMenuTable.delegate = self;
            basicMenuTable.dataSource = self;
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                basicMenuTable.scrollEnabled = false;
                webtop.scrollView.scrollEnabled = false;
                webbottom.scrollView.scrollEnabled = false;
                
                [_SquareView removeFromSuperview];
                [table removeFromSuperview];
                [Webview removeFromSuperview];
                [self.view addSubview:MenuView];
                [_ScrollView addSubview:basicMenuTable];
                [self.view addSubview:_ScrollView];
                _ScrollView.scrollEnabled = true;
                
            }
            else
            {
                basicMenuTable.scrollEnabled = false;
                webtop.scrollView.scrollEnabled = false;
                if(!DoubleTap)
                {
                DoubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleDoubleTap:)];
                }
                DoubleTap.numberOfTouchesRequired = 2;
                [DoubleTap setDelegate:self];
                [DetailWebView addGestureRecognizer:DoubleTap];
                
                [_SquareView removeFromSuperview];
                [table removeFromSuperview];
                [Webview removeFromSuperview];
                basicMenuTable.scrollEnabled = false;
                [_ScrollView addSubview:webtop];
                [_ScrollView addSubview:basicMenuTable];
                [MenuView addSubview:_ScrollView];
                [MenuView addSubview:DetailWebView];
                [MenuView addSubview:IMGSEPERATOR];
                [self.view addSubview:MenuView];
                _ScrollView.contentSize = CGSizeMake(320, basicMenuTable.contentSize.height);
                UILongPressGestureRecognizer *lpgr = [[[UILongPressGestureRecognizer alloc]
                                                      initWithTarget:self action:@selector(handleLongPress:)]autorelease];
                lpgr.minimumPressDuration = 2.0; //seconds
                lpgr.delegate = self;
                [basicMenuTable addGestureRecognizer:lpgr];
                //[lpgr release];
            }
            
        }
        
    }
    
    if(isCalledFromMenu)
    {
        backitem.hidden = false;
        backitem.enabled = true;
    }
     [self.view bringSubviewToFront:tabbar];
}


-(void)LoadSquareMenu :(NSURL*)path :(int)index
{
    isBasicMenu = false;
    
    if ( WebTopSquare != nil )
    {
        [WebTopSquare removeFromSuperview];
    }
    [carousel1 reloadData];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];

    NSString *dataPath1 = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSString *tabdataPath;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/%@_2_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    else
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/%@_3_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    
    NSLog(@"JSON Path %@",tabdataPath);
    NSString* content = [NSString stringWithContentsOfFile:tabdataPath encoding:NSUTF8StringEncoding error:NULL];
    
    if (content != nil)
    {
        arrmenuItems = [content JSONValue];
        
        _PageDots.numberOfPages =  [arrmenuItems count]-1;
        NSLog(@"Menu item count %d, first menu %@",[arrmenuItems count], [[arrmenuItems objectAtIndex:1]valueForKey:@"MenuItemName"]);
        NSMutableDictionary *menuItemDict = [arrmenuItems objectAtIndex:0];
        NSString* menuItemBgColor = [self getValueFromDictionary:menuItemDict:@"BackGroundColor"];
        NSString* menuItemBgIphoneImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIphoneImage"];
        NSString* menuItemBgIpadImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIpadImage"];
        BOOL menutabTopbar =[[self getValueFromDictionary:menuItemDict:@"MenuTopBar"] boolValue];

      
    if (menutabTopbar ==NO && (!isCalledFromMenu))
    {
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            CGFloat size = [self CheckDeviceSize];

            _SquareView.frame = CGRectMake(0, 0, 320, 412+size);
            
            if(!WebTopSquare)
            WebTopSquare = [[UIWebView alloc]initWithFrame:CGRectMake(0,0, 320, 44)];
            _PageDots.frame = CGRectMake(141, screenHeight - tabbar.frame.size.height - 50, 38, 40);
        }
        else
        {
            _SquareView.frame = CGRectMake(0, 0, 1024, 700);

            if(!WebTopSquare)
            WebTopSquare= [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 1024, 80)];
        }

        if ([menuItemBgIphoneImage isEqualToString:@""] && [menuItemBgIpadImage isEqualToString:@""] )
        {
            _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
   
        }
        else
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                if([menuItemBgIphoneImage isEqualToString:@""])
                {
                    _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
                }
                else
                {
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIphoneImage]];
                    _SquareView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:imgPath]];
                }
            }
            else
            {
                if([menuItemBgIpadImage isEqualToString:@""])
                {
                    _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
                }
                else
                {
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIpadImage]];
                    _SquareView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:imgPath]];
                    
                }

            }
            
            
        }
        WebTopSquare.opaque = NO;
        WebTopSquare.backgroundColor = [UIColor clearColor];
        WebTopSquare.delegate = self;
        NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        [WebTopSquare loadRequest:request];
       
        
        [table removeFromSuperview];
        [Webview removeFromSuperview];
        [_SquareView addSubview:WebTopSquare];
        [self.view addSubview:_SquareView];
        ////////////[_SquareView addSubview:carousel1];
        [carousel1 reloadData];
        [self.view bringSubviewToFront:tabbar];

    }
    

    else
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            CGFloat size = [self CheckDeviceSize];

            _SquareView.frame = CGRectMake(0, 44, 320, (412-44)+size);
            if(!WebTopSquare)
            WebTopSquare = [[UIWebView alloc]initWithFrame:CGRectMake(0,0, 320, 44)];
            _PageDots.frame = CGRectMake(141, screenHeight - tabbar.frame.size.height - 100, 38, 40);
        }
        else
        {
            _SquareView.frame = CGRectMake(0, 44, 1024, 700-44);
            if(!WebTopSquare)
            WebTopSquare= [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 1024, 80)];
        }

        if ([menuItemBgIphoneImage isEqualToString:@""] && [menuItemBgIpadImage isEqualToString:@""] )
        {
            _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
            
        }
        else
        {
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                if([menuItemBgIphoneImage isEqualToString:@""])
                {
                    _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
                }
                else
                {
                    CGFloat size = [self CheckDeviceSize];
                    
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIphoneImage]];
                    UIImageView *imgbackground = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgPath]]autorelease];
                    imgbackground.frame = CGRectMake(0, 0, 320, 368+size);
                    
                    [_SquareView addSubview:imgbackground];
                }
            }
            else
            {
                if([menuItemBgIpadImage isEqualToString:@""])
                {
                    _SquareView.backgroundColor = [self colorForHex:menuItemBgColor];
                }
                else
                {
                    NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/Images/%@",appid,[arrapptabid objectAtIndex:index],menuItemBgIpadImage]];
                    UIImageView *imgbackground = [[[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:imgPath]]autorelease];
                    imgbackground.frame = CGRectMake(0, 0, 1024,700); //_SquareView.frame;
                    
                    [_SquareView addSubview:imgbackground];
                }
            }
        }
        
        WebTopSquare.opaque = NO;
        WebTopSquare.backgroundColor = [UIColor clearColor];
        WebTopSquare.delegate = self;
        NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        [WebTopSquare loadRequest:request];
        [table removeFromSuperview];
        [Webview removeFromSuperview];
        [_SquareView addSubview:carousel1];
        [_SquareView addSubview:WebTopSquare];
        [self.view addSubview:_SquareView];
        carousel1.scrollToItemBoundary = YES;
        [carousel1 reloadData];
        [self.view bringSubviewToFront:tabbar];
        
    }

    }
    int itemIndex = 0;
    NSNumber *savedIndex = [dictSquareMenuSelectedItems objectForKey:[NSNumber numberWithInt:z]];
    if(savedIndex != nil)
    {
        itemIndex = [savedIndex intValue];
    }
    if(itemIndex < [carousel1 numberOfItems] )
    {
        [carousel1 scrollToItemAtIndex:itemIndex animated:YES];
    }
    [_SquareView bringSubviewToFront:_PageDots];
    
    if(isCalledFromMenu)
    {
        backitem.hidden = false;
        backitem.enabled = true;
    }
 [self.view bringSubviewToFront:tabbar];
}


-(void)LoadCameraFormTab :(NSURL*)path :(int)index
{
     [locationManager startUpdatingLocation];
    //Reset camFormSelectedImage to nil
    camFormSelectedImage = nil;
    
    if(_ScrollView != nil)
    {
        [_ScrollView removeFromSuperview];
    }
    if(!_ScrollView)
    _ScrollView = [[UIScrollView alloc]init];
    _ScrollView.frame = CGRectMake(0, 44, 320, 780);
    
    _ScrollView.scrollEnabled = true;
    _ScrollView.bounces = true;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *dataPath1 = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSString *tabdataPath;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/%@_2_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    else
    {
        tabdataPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@/%@_3_%@.txt",appid,[arrapptabid objectAtIndex:index],appid,[arrapptabid objectAtIndex:index]]];
    }
    if(!cameraFormView)
    cameraFormView = [[UIView alloc]init];
    
    if(!cameraFormWebView)
    cameraFormFieldsView = [[UIView alloc]init];
    
    if ( cameraFormWebView != nil )
    {
        [cameraFormWebView removeFromSuperview];
    }
    
    NSString* content = [NSString stringWithContentsOfFile:tabdataPath encoding:NSUTF8StringEncoding error:NULL];
    
    NSLog(@"JSON: %@",content);
    
    dictCameraForm = [content JSONValue];
    
    NSMutableDictionary *dictCamProperties = [[dictCameraForm valueForKey:@"cameraTabItem"] objectAtIndex:0];
    
    NSMutableArray *arrCameraItems = [dictCameraForm valueForKey:@"tabCameraDataItem"];
    
    arrCameraFormFields = arrCameraItems;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
       // CGFloat size = [self CheckDeviceSize];
        cameraFormView.frame = CGRectMake(0, 0, 320, 3500);
        
//        //Add preview imageView
        if(!camFormImageView)
        {
        camFormImageView = [[UIImageView alloc]
                              initWithFrame:CGRectMake(0, 0, 320, 240)];
        }
        camFormImageView.clipsToBounds = true;
        [cameraFormView addSubview:camFormImageView];
        camFormImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        NSString *imgPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/Images/",appid,[arrapptabid objectAtIndex:index]]];
        
        //Set background
        NSString *bkgImgPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"BackGroundIphoneImage"]]];
        
        NSString *bkgColor = [dictCamProperties valueForKey:@"BackGroundColor"];
        if([bkgColor isEqualToString:@""])
        {
            bkgColor = @"#FFFFFF";
        }
        
        if([[dictCamProperties valueForKey:@"BackGroundIphoneImage"] isEqualToString:@""])
        {
            cameraFormView.backgroundColor = [self colorForHex:bkgColor];
        }
        else
        {
//            Basicimgbackground = [[UIImageView alloc] initWithImage:[[UIImage imageWithContentsOfFile:bkgImgPath]stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0]];
//            [cameraFormView addSubview:Basicimgbackground];
            cameraFormView.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:bkgImgPath]];
        }

        //Set title
        NSString *formTitle = [dictCamProperties valueForKey:@"Title"];
        lbl.text = formTitle;
        
        NSString *previewImgPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"PreviewImage"]]];
        
        NSString *takePhotoImgPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"TakePhoto"]]];
        
        NSString *choosePhotoImgPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"ChoosePhoto"]]];
        
        NSString *sendButtonImgPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"SendButton"]]];
        
        okButtonPath = [imgPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[dictCamProperties valueForKey:@"OKButton"]]];;
        
        UIImage *img = [UIImage imageWithContentsOfFile:previewImgPath];

        camFormImageView.image = img;
        
//        if(img.size.width > 320 )
//        {
//            float ratio = img.size.width / 320;
//            camFormImageView.frame = CGRectMake(camFormImageView.frame.origin.x, camFormImageView.frame.origin.y,
//                                         320, img.size.height/ratio);
//            
//        }
        
        float buttonX, buttonY, buttonW, buttonH, buttonPadding;
        buttonX = 20;
        buttonY = camFormImageView.frame.size.height + 20;
        buttonW = 280;
        buttonH = 40;
        buttonPadding = 10;
        
        //Add buttons
        UIButton *takePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [takePhotoButton addTarget:self
                   action:@selector(takePicture:)
         forControlEvents:UIControlEventTouchDown];
        //[takePhotoButton setTitle:@"Take Photo" forState:UIControlStateNormal];
        [takePhotoButton setBackgroundImage:[UIImage imageWithContentsOfFile:takePhotoImgPath] forState:UIControlStateNormal];
        takePhotoButton.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
        
        UIButton *choosePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [choosePhotoButton addTarget:self
                            action:@selector(choosePicture:)
                  forControlEvents:UIControlEventTouchDown];
        //[choosePhotoButton setTitle:@"Choose Photo" forState:UIControlStateNormal];
        [choosePhotoButton setBackgroundImage:[UIImage imageWithContentsOfFile:choosePhotoImgPath] forState:UIControlStateNormal];
        
        choosePhotoButton.frame = CGRectMake(buttonX, buttonY + buttonH + buttonPadding, buttonW, buttonH);
               
        [cameraFormView addSubview:takePhotoButton];
        [cameraFormView addSubview:choosePhotoButton];

        float cameraFormWebViewTop = (buttonY + 2*(buttonH + buttonPadding)), cameraFormWebViewH = 300;
        if(!cameraFormWebView)
        cameraFormWebView = [[UIWebView alloc]initWithFrame:CGRectMake(0,cameraFormWebViewTop, 320, cameraFormWebViewH)];
        [cameraFormView addSubview:cameraFormWebView];
        
        cameraFormWebView.opaque = NO;
        cameraFormWebView.backgroundColor = [UIColor clearColor];
        cameraFormWebView.delegate = self;
        NSURLRequest * request = [NSURLRequest requestWithURL: path
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        [cameraFormWebView loadRequest:request];
        
        float fieldViewTop = cameraFormWebViewTop + cameraFormWebViewH + 22;
        
        cameraFormFieldsView.frame = CGRectMake(0, fieldViewTop, 320, 2000);
        [cameraFormView addSubview:cameraFormFieldsView];
        

        [self loadFormFields:arrCameraItems:sendButtonImgPath : dictCamProperties];
        

        [_ScrollView addSubview:cameraFormView];
        [self.view addSubview:_ScrollView];
        
        [self.view bringSubviewToFront:cameraFormFieldsView];
        //Bring the bottom tab bar on top of everything
        [self.view bringSubviewToFront:tabbar];

    }
    else
    {
        //UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Unsupported Tab" message:@"This type of tab is not supported for iPad devices. Please contact system administrator for further details." delegate:self cancelButtonTitle:@"Cancel"otherButtonTitles:@"", ..., nil]
    }
    
    }

-(void) loadFormFields:(NSMutableArray*) formFields : (NSString*) sendButtonImgPath  : (NSMutableDictionary*) dictCamProperties
{
    NSInteger loop; // Array index variable
    NSInteger yOffset = 30; // vertical distance between each row of UITextViews
    
    int x2Spacer = 10; // Pixel ammount bewteen right edge of main view and right edge of textview
    int yTextFieldOffset = 29; // Pixel amount for upwards offset between textview and associated label
    
    // UIView starting CGRect
    // Some amounts change when a used in a different main view
    int xMainView = 0;
    int widthMainView = 320;
    int heightMainView = 570; // Region of main view should not overlap 'UI Parts' created in Storyboard to avoid interaction with their behaviors
    
    // UILabel starting CGRect
    // Some amounts change when a used in a different main view
    int xLabel = 10;
    int yLabel = 0;
    int widthLabel = 40;
    int heightLabel = 31;
    
    // UITextView starting CGRect
    // Some amounts change when a used in a different main view
    int xTextField = xLabel ; //+ widthLabel + xSpacer;
    int yTextField = yLabel ;
    int widthTextField = widthMainView - xTextField - x2Spacer;
    int heightTextField = 30;
    
    //Get style
    NSString *Fontsize = [dictCamProperties valueForKey:@"FontSize"];
    NSString *FontStyle = [dictCamProperties valueForKey:@"FontStyle"];
    NSString *FontStyleType = [dictCamProperties valueForKey:@"FontStyleType"];
    NSString *Textcolor = [dictCamProperties valueForKey:@"Textcolor"];
    
    CGRect mainViewRect = CGRectMake(xMainView, 0, widthMainView, heightMainView);
    UIView* mainView = [[[UIView alloc] initWithFrame:mainViewRect]autorelease];

    [cameraFormFieldsView addSubview: mainView];
    if(!arrFormFieldObjects)
    {
       arrFormFieldObjects = [[NSMutableArray alloc]init];
    }
    cameraFormHeight = 0;
    for ( loop = 0;  loop< [formFields count]; loop++ ) {
        
        heightTextField = 30;
        
        NSMutableDictionary *fieldProperties = [formFields objectAtIndex:loop];
        NSString *fieldName = [self getValueFromDictionary:fieldProperties :@"Itemlabel"];
        NSString *fieldValue = [self getValueFromDictionary:fieldProperties :@"Value"];
        NSString *fieldType = [self getValueFromDictionary:fieldProperties :@"Itemtype"];
        int noOfLines = [[self getValueFromDictionary:fieldProperties :@"NoofLines"] intValue];
        
        // Create UILabel
        CGRect labelFrame = CGRectMake(xLabel, yLabel+5, widthTextField, heightLabel);
        UnderLineLabel *label = [[[UnderLineLabel alloc] initWithFrame: labelFrame]autorelease];
        [label setText:fieldName];
        //[label sizeToFit];
        label.backgroundColor = [UIColor clearColor];
        
        if (![Fontsize isEqualToString:@""] && ![FontStyle isEqualToString:@""]) {
            label.font = [UIFont fontWithName:FontStyle size:[Fontsize floatValue]];
            label.textColor = [self colorForHex:Textcolor];
            if([FontStyleType hasSuffix:@"Underline"])
            {
                label.startFromZero = true;
                label.underline = true;
            }
        }
        
        [mainView addSubview:label];
        // NSLog(@"UILabel 'label' is: %@", label);
        
        if([fieldType isEqualToString:@"S"])
        {
            // Create UITextField
            UITextFieldExt *textField = [[[UITextFieldExt alloc] initWithFrame:CGRectMake(xTextField, yTextField + yTextFieldOffset, widthTextField, heightTextField)]autorelease];
            // NSLog(@"UITextField 'textField' is: %@\n\n", textField);
            // List of 'UITextField'properties used in this method
            [textField.layer setBorderColor: [[UIColor grayColor] CGColor]];
            [textField.layer setBorderWidth: 1.0];
            [textField.layer setCornerRadius:4.0f];
            [textField.layer setMasksToBounds:YES];
            
            textField.font = [UIFont systemFontOfSize:15];
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.returnKeyType = UIReturnKeyDone;
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            textField.returnKeyType = UIReturnKeyDefault;
            textField.delegate = self;
            textField.text = fieldValue;
            textField.backgroundColor = [UIColor whiteColor];
            [mainView addSubview:textField];
            
            //textField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            
            [arrFormFieldObjects addObject:textField];
        }
        else
        {
            heightTextField = (heightTextField * noOfLines);
            // Create UITextField
            UITextView *textField = [[[UITextView alloc] initWithFrame:CGRectMake(xTextField, yTextField + yTextFieldOffset, widthTextField, heightTextField)]autorelease];

            // NSLog(@"UITextField 'textField' is: %@\n\n", textField);
            // List of 'UITextField'properties used in this method
            [textField.layer setBorderColor: [[UIColor grayColor] CGColor]];
            [textField.layer setBorderWidth: 1.0];
            [textField.layer setCornerRadius:4.0f];
            [textField.layer setMasksToBounds:YES];
            
            textField.font = [UIFont systemFontOfSize:15];
            textField.autocorrectionType = UITextAutocorrectionTypeNo;
            textField.keyboardType = UIKeyboardTypeDefault;
            textField.returnKeyType = UIReturnKeyDefault;
            textField.delegate = self;
            textField.backgroundColor = [UIColor whiteColor];
            textField.text = fieldValue;

            [mainView addSubview:textField];
            
            [arrFormFieldObjects addObject:textField];
        }
        
        
        yLabel = yLabel + yOffset + heightTextField; // Move label down
        yTextField = yLabel ; //+ yTextField+yOffset;// Move UITextField down
        
        NSLog(@" Y: %d",yTextField);
        
    }

    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sendButton addTarget:self
                          action:@selector(submitForm:)
                forControlEvents:UIControlEventTouchDown];
    //[choosePhotoButton setTitle:@"Choose Photo" forState:UIControlStateNormal];
    [sendButton setBackgroundImage:[UIImage imageWithContentsOfFile:sendButtonImgPath] forState:UIControlStateNormal];
    sendButton.frame = CGRectMake(20, yLabel+30, 280, 40);
    [mainView addSubview:sendButton];
    
    cameraFormHeight = yLabel + 100;
    
    mainView.frame = CGRectMake(mainView.frame.origin.x, mainView.frame.origin.y, mainView.frame.size.width, cameraFormHeight);
    
}

-(void) takePicture:(id) sender
{
    UIImagePickerController *imagePicker = [[[UIImagePickerController alloc] init]autorelease];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    [imagePicker setDelegate:self];
    
    [self presentModalViewController:imagePicker animated:YES];
}


-(void) choosePicture:(id) sender
{
    UIImagePickerController *imagePicker = [[[UIImagePickerController alloc] init]autorelease];
    
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
   
    [imagePicker setDelegate:self];
    
    [self presentModalViewController:imagePicker animated:YES];
}

-(void) submitForm:(id) sender
{
    [[self.view findFirstResponder] resignFirstResponder];
    NSNumber *num = [NSNumber numberWithInt:z];
    if(camFormSelectedImage == nil)
    {
       
        UIAlertView *alert = [[[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:@"Ta eller välj en bild"
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil]autorelease];
        [alert show];
    }
    else
    {
//        acc = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//        acc.hidesWhenStopped  = true;
//        acc.color = [UIColor blackColor];
//        
//        acc.frame = CGRectMake(150, 200, 30, 30);
//        [self.view addSubview:acc];
//        [self.view bringSubviewToFront:acc];
//        [acc startAnimating];
        
        [self showBlackViewLoader];
        [self performSelectorInBackground:@selector(PostData:) withObject:num];
    
    }
}


//Detect 3G or Wifi network
- (BOOL) connectedToWifi
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status == ReachableViaWiFi)
    {
        //WiFi
        return YES;
    }
    else if (status == ReachableViaWWAN)
    {
        //3G
        return NO;
    }
}


- (void) PostData:(NSNumber*) index
{
    NSString *returnString;
    NSData *imageData;
    if ([self connectedToNetwork])
    {
        if ([self connectedToWifi])
        {
            imageData = UIImageJPEGRepresentation([camFormImageView image], 1.0);
        }
        else
        {
            //Resize image
                CGSize newSize = CGSizeMake(800.0f, 800.0f);
                UIGraphicsBeginImageContext(newSize);
                [[camFormImageView image] drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
                UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                imageData = UIImageJPEGRepresentation(newImage, 1.0);
        }
        
        NSString *urlString = [NSString stringWithFormat:@"%@UploadImage.aspx",serverURL];
        NSURL *url = [NSURL URLWithString:urlString];
        NSString *filename = @"userfile";
        NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] initWithURL:url]autorelease];
        
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *postbody = [NSMutableData data];
        
        //NSString *postData = @"";
        
        [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"hTabid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[arrapptabid objectAtIndex:z] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"hLatitude\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[latitude  dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"hLongitude\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[longitude  dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"hTabJson\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSInteger loop; // Array index variable
        //Get data from fields
        for ( loop = 0;  loop< [arrCameraFormFields count]; loop++ )
        {
            NSMutableDictionary *fieldProperties = [arrCameraFormFields objectAtIndex:loop];
            
            NSString *fieldType = [self getValueFromDictionary:fieldProperties :@"Itemtype"];
            NSString *fieldValue;
            
            if([fieldType isEqualToString:@"S"])
            {
                UITextField *tf = (UITextField*)[arrFormFieldObjects objectAtIndex:loop];
                fieldValue = tf.text;
            }
            else
            {
                UITextView *tf = (UITextView*)[arrFormFieldObjects objectAtIndex:loop];
                fieldValue = tf.text;
            }
            
            [fieldProperties setValue:fieldValue forKey:@"Value"];
            
            [arrCameraFormFields replaceObjectAtIndex:loop withObject:fieldProperties];
        }
        
        //    NSMutableDictionary *formFields = [[NSMutableDictionary alloc]init];
        //    [formFields setValue:(arrCameraFormFields) forKey:@"tabCameraDataItem"];
        
        NSString *jsonString = [arrCameraFormFields JSONRepresentation];
        [postbody appendData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //[postbody appendData:[postData dataUsingEncoding:NSUTF8StringEncoding]];
        if(imageData != nil)
        {
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filetype=\"image/jpeg\"; filename=\"%@.jpg\"\r\n", filename] dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postbody appendData:[NSData dataWithData:imageData]];
            
            [postbody appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [request setHTTPBody:postbody];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        returnString = [[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]autorelease];
       // [request release];
        
        //NSLog(returnString);
        
        [self performSelectorOnMainThread:@selector(OnPostDataCompleted) withObject:nil waitUntilDone:YES];
    }
    else
    {
        UIAlertView *alert = [[[UIAlertView alloc]
                              initWithTitle:@"Felmeddelande"
                              message:@"Uppkoppling till internet saknas, meddelandet har inte skickats."
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil]autorelease];
		[alert show];
    }
}

-(void) OnPostDataCompleted
{
    //[acc stopAnimating];
    if(!confirmationMessageView)
    {
       confirmationMessageView = [[UIView alloc]init];
    }
    confirmationMessageView.frame = CGRectMake(0,40,320,screenHeight-40);
    if(!confirmationMessageWebView)
    {
        confirmationMessageWebView = [[UIWebView alloc]initWithFrame:CGRectMake(5,45, 310, screenHeight-200)];
    }
    confirmationMessageView.layer.borderColor = [UIColor blackColor].CGColor;
    confirmationMessageView.layer.borderWidth = 1.0f;
    confirmationMessageView.layer.cornerRadius = 3.0f;
    confirmationMessageView.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.95f];
    
   
    confirmationMessageWebView.opaque = NO;
    confirmationMessageWebView.backgroundColor = [UIColor clearColor];
    confirmationMessageWebView.delegate = self;
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath1 = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    
    
    NSString *taPath = [dataPath1 stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@/PopBoxHTML.html",appid,[arrapptabid objectAtIndex:z]]];
    
    NSURLRequest * request = [NSURLRequest requestWithURL: [NSURL fileURLWithPath:taPath]
                                               cachePolicy: NSURLRequestUseProtocolCachePolicy
                                           timeoutInterval: 1000];

    [confirmationMessageWebView loadRequest:request];
    
    //Add buttons
    UIButton *okButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [okButton addTarget:self
                        action:@selector(hideConfirmationMessage:)
              forControlEvents:UIControlEventTouchDown];
    [okButton setTitle:@"" forState:UIControlStateNormal];
    okButton.frame = CGRectMake(5, screenHeight- 120, 310, 40);
    
    [okButton setBackgroundImage:[UIImage imageWithContentsOfFile:okButtonPath] forState:UIControlStateNormal];
//    [okButton setBackgroundColor:[UIColor blackColor]];
    
    [confirmationMessageView addSubview:confirmationMessageWebView];
    [confirmationMessageView addSubview:okButton];
    
    [self.view addSubview:confirmationMessageView];
    
    
    [confirmationMessageView setFrame:CGRectMake(0, screenHeight, 320, screenHeight)];
    [confirmationMessageView setBounds:CGRectMake(0, 40, 320, screenHeight)];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationDelegate:self];
    [confirmationMessageView setFrame:CGRectMake(0, 40, 320, screenHeight)];
    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:confirmationMessageView];
}

-(void) hideConfirmationMessage:(id) sender
{
    //[confirmationMessageView removeFromSuperview];
    
    [confirmationMessageView setFrame:CGRectMake(0, 40, 320, screenHeight)];
    [confirmationMessageView setBounds:CGRectMake(0, 40, 320, screenHeight)];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.5];
    [UIView setAnimationDelegate:self];
    [confirmationMessageView setFrame:CGRectMake(0, screenHeight, 320, screenHeight)];
    [UIView commitAnimations];
}


- (void) resizeCameraFormImage
{
    
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSLog(@"gestureRecognizer shouldReceiveTouch: tapCount = %d",(int)touch.tapCount);
    if (touch.tapCount ==2) {
        self.gestureState = UIGestureRecognizerStateBegan;
        [self handleDoubleTap:gestureRecognizer];
        return YES;
    }
    
    return NO;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
   
    self.gestureState = gestureRecognizer.state;
    return YES;
}

// Handler will be called from timer
- (void)handleDoubleTap:(UIGestureRecognizer*)sender {
    NSLog(@"handleSingleTap, state: %@", [self debugUIGestureState:self.gestureState]);
//    if ([StrFullScreen isEqualToString:@"NO"]) {
//        fullframe = DetailWebView.frame;
//         DetailWebView.frame = CGRectMake(0, 0, 1024, 728);
//        StrFullScreen = @"YES";
//    }
//    else
//    {
//        DetailWebView.frame = fullframe;
//        StrFullScreen = @"NO";
//    }
//    
//    CGPoint p = [sender locationInView:basicMenuTable];
//    
//    NSIndexPath *indexPath = [basicMenuTable indexPathForRowAtPoint:p];
//    if (indexPath == nil)
//        NSLog(@"long press on table view but not on a row");
//    else
//        NSLog(@"long press on table view at row %d", indexPath.row);
    
    detailWebViewURLHistoryCount = 0;
    
    if(!isCalledFromBasiciPad)
    {
        DetailWebView.hidden = true;
        if(!DetailWebView)
        {
        DetailWebView =  [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, 1024, 728)];
        }
        [MenuView addSubview:DetailWebView];
        
        
        DetailWebView.opaque = NO;
        DetailWebView.backgroundColor = [UIColor clearColor];
        DetailWebView.delegate = self;
        [DetailWebView addGestureRecognizer:DoubleTap];

        NSURL *url =[NSURL URLWithString:fullScreenURL];
        NSURLRequest * request = [NSURLRequest requestWithURL: url
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        [DetailWebView loadRequest:request];

        IMGSEPERATOR.hidden = true;
        [MenuView bringSubviewToFront:DetailWebView];
        tabbar.hidden = true;
        isCalledFromBasiciPad = true;
        DetailWebView.hidden = false;
    }
    else
    {
        NSLog(@"%f   %f",detailWebViewFrame.size.width,detailWebViewFrame.size.height);
        DetailWebView.frame = detailWebViewFrame;
        [self resizeDetailView];
        isCalledFromBasiciPad = false;
        IMGSEPERATOR.hidden = false;
        tabbar.hidden = false;
        [MenuView bringSubviewToFront:DetailWebView];
    }
   
}

- (NSString*) debugUIGestureState: (UIGestureRecognizerState) state {
    NSString *strgesture;
    switch (state) {
        case UIGestureRecognizerStateBegan:strgesture=@"UIGestureRecognizerStateBegan";break;
        case UIGestureRecognizerStateEnded:strgesture=@"UIGestureRecognizerStateEnded";break;
        case UIGestureRecognizerStateFailed:strgesture=@"UIGestureRecognizerStateFailed";break;
        case UIGestureRecognizerStateChanged:strgesture=@"UIGestureRecognizerStateChanged";break;
        case UIGestureRecognizerStatePossible:strgesture=@"UIGestureRecognizerStatePossible";break;
        case UIGestureRecognizerStateCancelled:strgesture=@"UIGestureRecognizerStateCancelled";break;
        default:
            break;
    }
    return strgesture;
}

-(void)getrss :(NSNumber*)num
{
    int inn = [num intValue];
    [self downloadDataForTabSynchronized:appid :[arrapptabid objectAtIndex:inn]];
    
    [self performSelectorOnMainThread:@selector(reloadwebForRSS) withObject:nil waitUntilDone:YES];
    
}

-(void)reloadweb
{
    [Webview reload];
}

-(void)reloadwebForRSS
{
    NSString* strURL = [arrhtmlpaths objectAtIndex:z];
    NSURL* nsURLRSS = [ NSURL fileURLWithPath :strURL];
    NSURLRequest * request = [NSURLRequest requestWithURL: nsURLRSS
                                              cachePolicy: NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval: 1000];
    [Webview loadRequest:request];
    actview.backgroundColor =[UIColor clearColor];
    [actview removeFromSuperview];

    NSLog(@"RSS reloaded");

}

-(void)getlostdata :(NSNumber*)num
{
    int index = [num intValue];
    [self downloadDataForTabSynchronized:appid :[arrtabid objectAtIndex:index]];
    [actview removeFromSuperview];
    actview.backgroundColor =[UIColor clearColor];
    str = [arrhtmlpaths objectAtIndex:index];
    nsURL = [ NSURL fileURLWithPath :str];
    str = [nsURL absoluteString];
    NSString  *strabsolute  =[str stringByAppendingFormat:[NSString stringWithFormat:@"?IMEI=%@",strUDID]];
    nsURL =[NSURL URLWithString:strabsolute];
    NSURLRequest * request = [NSURLRequest requestWithURL: nsURL
                                              cachePolicy: NSURLRequestUseProtocolCachePolicy
                                          timeoutInterval: 1000];
    [Webview loadRequest:request];
    check = 0;
}

-(void)reloadwebview :(id)sender 
{
    
    if(DetailWebView.canGoBack && isCalledFromBasiciPad && useHistory)
    {
        [DetailWebView goBack];
    }
    else if (Webview.canGoBack)
    {
        NSURLRequest * request = [NSURLRequest requestWithURL: [NSURL URLWithString:@""]
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        //[DetailWebView loadRequest:request];
        if(isCalledFromMenu)
        {
            if(useHistory)
            {
                [Webview goBack];
            }
            else
            {
                lastTabTypeId = 0;
                lastTabIndex = 0;
                int icount = [arrMenuTabsNavigated count];
                if(icount > 0)
                {
                    lastTabIndex = [[arrMenuTabsNavigated objectAtIndex:icount-1] intValue];
                    lastTabTypeId = [[arrapptabtypeid objectAtIndex:lastTabIndex] intValue];
                }
                if(isCalledFromBasiciPad)
                {
//                    isCalledFromMenu = false;
//                    [self createURLAndLoad:lastBasicTabIdiPad : 0];
//                    [self handleDoubleTap:DetailWebView];
                }
                else if(lastTabTypeId == 12 || lastTabTypeId == 13) //back for menu tab
                {
                    isCalledFromMenu = false;
                    [self createURLAndLoad:lastTabIndex : 0];
                    [backitem setHidden:YES];
                    backitem.enabled = NO;
                    lastTabTypeId = 0;
                    lastTabIndex = 0;
                    [arrMenuTabsNavigated removeLastObject];
                }

            }
        }
        else
        {
            [Webview goBack];
        }
    }
    else if (!Webview.canGoBack) 
    {
        NSURLRequest * request = [NSURLRequest requestWithURL: [NSURL URLWithString:@""]
                                                  cachePolicy: NSURLRequestUseProtocolCachePolicy
                                              timeoutInterval: 1000];
        //[DetailWebView loadRequest:request];
        lastTabTypeId = 0;
        lastTabIndex = 0;
        int icount = [arrMenuTabsNavigated count];
        if(icount > 0)
        {
            lastTabIndex = [[arrMenuTabsNavigated objectAtIndex:icount-1] intValue];
            lastTabTypeId = [[arrapptabtypeid objectAtIndex:lastTabIndex] intValue];
        }
        if(isCalledFromBasiciPad)
        {
            //isCalledFromMenu = false;
            //[self createURLAndLoad:lastBasicTabIdiPad : 0];
            [self handleDoubleTap:DetailWebView];
            if(icount == 0 )
            {
                [backitem setHidden:YES];
                backitem.enabled = NO;
            }
        }
        else if(lastTabTypeId == 12 || lastTabTypeId == 13) //back for menu tab
        {
            isCalledFromMenu = false;
            [self createURLAndLoad:lastTabIndex : 0];
            [backitem setHidden:YES];
            backitem.enabled = NO;
            lastTabTypeId = 0;
            lastTabIndex = 0;
            [arrMenuTabsNavigated removeLastObject];
        }
        else
        {
            [backitem setHidden:YES];
            backitem.enabled = Webview.canGoBack;
        }
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    [self.view bringSubviewToFront:tabbar];
    if (webView == DetailWebView) {
        fullScreenURL = [[request URL] absoluteString];
    }
    
    if ([[[request URL] scheme] isEqual:@"mailto"])
    {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    else 
    {
        NSString *requestString = [[request URL] absoluteString];
        if ([requestString  hasPrefix:@"http://" ] || [requestString  hasPrefix:@"https://" ] || [requestString isEqualToString:@"about:blank"])
        {
            webView.scalesPageToFit = YES;
        }
        else 
        {
            webView.scalesPageToFit = NO;
        }
        
        if([[arrapptabtypeid objectAtIndex:z] intValue] == 9)  //Form tab, disable zooming
        {
            webView.scalesPageToFit = NO;
        }
        
        if (navigationType==UIWebViewNavigationTypeLinkClicked && ![requestString  hasPrefix:@"tel:"])
        {
            NSString *requestString = [[request URL] absoluteString];
            
            NSArray *components = [requestString componentsSeparatedByString:@"?"];
            if ([components count] > 1 && [(NSString *)[components objectAtIndex:1]  hasPrefix:@"title"]) 
            {
                tii = @"";
                desc = @"";
                
                NSString *stt = [components objectAtIndex:1];
                NSArray *componen = [stt componentsSeparatedByString:@"&"];
                for (int i=0; i<[componen count]; i++) 
                {
                    NSString *str11 = [componen objectAtIndex:i];
                    if ([str11  hasPrefix:@"title:"] && [str11 length] > 6)
                    {
                        tii = [str11 substringFromIndex:6];
                    }
                    else if ([str11  hasPrefix:@"discription:"] && [str11 length] > 13)
                    {
                        desc = [str11 substringFromIndex:13];
                    }
                    else if ([str11  hasPrefix:@"date:"] && [str11 length] > 5)
                    {
                        ddd = [str11 substringFromIndex:5];
                    }
                }
                [self addEvent:tii :desc :ddd];
                return NO;
            }
        }
        if (navigationType == UIWebViewNavigationTypeBackForward) 
        {
            if (webView ==Webview) {
                
            
            if (![webView canGoBack]) 
            {
                [backitem setHidden:YES];
                backitem.enabled = NO;
            }
            else 
            {
                [backitem setHidden:NO];
                backitem.enabled = YES;
            }
                }
        }
        if (navigationType == UIWebViewNavigationTypeOther) 
        {
            NSString *requestString = [[request URL] absoluteString];
            if ([requestString  hasPrefix:@"http://" ] || [requestString  hasPrefix:@"https://" ] || [requestString isEqualToString:@"about:blank"]) 
            {
                [backitem setHidden:NO];
                backitem.enabled = YES;
                useHistory = true;
            }
            else 
            {
                if(!isCalledFromMenu)
                {
                    [backitem setHidden:YES];
                    backitem.enabled = NO;
                }
                else
                {
                    if([[arrapptabtypeid objectAtIndex:z] intValue] == 2 )
                        useHistory = false;
                }
                
            }
        }
    }
    if([[arrapptabtypeid objectAtIndex:z] intValue] == 4 || [[arrapptabtypeid objectAtIndex:z] intValue] == 9)
    {
        [backitem setHidden:YES];
        backitem.enabled = NO;
    }
    NSLog(@"request.URL%@",request.URL);
    if (webView == DetailWebView)
    {
        detailWebViewURLHistoryCount++;
    }
    return  YES;

}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [acc removeFromSuperview];
    [self.view bringSubviewToFront:tabbar];
    if(isCalledFromBasiciPad)
    {

        [backitem setHidden:NO];
        backitem.enabled = YES;
        [loader removeFromSuperview];
        [acc removeFromSuperview];
    }
    else
    {
        if(webView ==webtop)
        {
            isTopLoaded = true;
            CGRect frame = webtop.frame;
                frame.size.height = 1;
                webtop.frame = frame;
                CGSize fittingSize = [webtop sizeThatFits:CGSizeZero];
                frame.size = fittingSize;
                webtop.frame = frame;
                NSLog(@"%f",webtop.frame.origin.y+webtop.frame.size.height);
                NSLog(@"%f",basicMenuTable.contentSize.height);
            
            
            if(isIPad)
            {
                  webtop.frame = CGRectMake(webtop.frame.origin.x, webtop.frame.origin.y, 320, webtop.frame.size.height);
                //basicMenuTable.frame = CGRectMake(basicMenuTable.frame.origin.x,webtop.frame.size.height, basicMenuTable.frame.size.width, basicMenuTable.frame.size.height-(webtop.frame.size.height-basicMenuTable.frame.origin.y));
                
                basicMenuTable.frame = CGRectMake(basicMenuTable.frame.origin.x, frame.size.height, basicMenuTable.frame.size.width,basicMenuTable.contentSize.height );
                
                detailWebViewFrame = CGRectMake(DetailWebView.frame.origin.x,0, DetailWebView.frame.size.width, _ScrollView.frame.size.height);
                NSLog(@"%f",basicMenuTable.contentSize.height);
                //_ScrollView.contentSize = CGSizeMake(320,basicMenuTable.contentSize.height+(basicMenuTable.contentSize.height/2));
                CGFloat maxheight = webtop.frame.size.height+basicMenuTable.contentSize.height+webbottom.frame.size.height;
                if(maxheight < screenHeight-90)
                {
                    maxheight = 680;
                    if([arrmenuItems count] >= 3)
                        maxheight += ([arrmenuItems count]-3)*40;
                }
                
                _ScrollView.contentSize = CGSizeMake(320, maxheight);
                
                //_ScrollView.frame = CGRectMake(basicMenuTable.frame.origin.x,0, basicMenuTable.frame.size.width, basicMenuTable.frame.size.height+webtop.frame.size.height);
                [_ScrollView addSubview:webtop];
                IMGSEPERATOR.frame = CGRectMake(320, 0, 2, 735);
                
                NSIndexPath *ii = [NSIndexPath indexPathForRow:0 inSection:0];
                [self tableView:basicMenuTable didSelectRowAtIndexPath:ii];
                
                [basicMenuTable selectRowAtIndexPath:ii animated:NO scrollPosition:UITableViewScrollPositionMiddle];
                
                [self.view bringSubviewToFront:tabbar];
                
            }
            else
            {
                basicMenuTable.frame = CGRectMake(basicMenuTable.frame.origin.x, frame.size.height, basicMenuTable.frame.size.width,basicMenuTable.contentSize.height );
                NSLog(@"%f",basicMenuTable.frame.origin.y);
                
                CGFloat maxheight = webtop.frame.size.height+basicMenuTable.contentSize.height+webbottom.frame.size.height;
                if(maxheight < screenHeight)
                {
                    maxheight = screenHeight * 1.05;
                }
                _ScrollView.contentSize = CGSizeMake(320, maxheight);
                //_ScrollView.contentSize = CGSizeMake(320, webtop.frame.size.height+basicMenuTable.contentSize.height+webbottom.frame.size.height+500);
                
            }
            [webbottom loadRequest:WebBottomRequest];
        }
        
        if (webView == webbottom) {
            isBottomLoaded = true;
            CGRect frame = webbottom.frame;
            CGFloat frameheight = frame.size.height;
            frame.size.height = 1;
            webbottom.frame = frame;
            CGSize fittingSize = [webbottom sizeThatFits:CGSizeZero];
            frame.size = fittingSize;
            frame.origin.y = frame.origin.y-(frame.size.height-frameheight);
            webbottom.frame = frame;
            
            //basicMenuTable.frame = CGRectMake(basicMenuTable.frame.origin.x, frame.size.height, basicMenuTable.frame.size.width,basicMenuTable.contentSize.height );
            
            frame.origin.y = webtop.frame.origin.y + basicMenuTable.contentSize.height+webtop.frame.size.height;
            webbottom.frame = frame;
            CGFloat maxheight = webtop.frame.size.height+basicMenuTable.contentSize.height+webbottom.frame.size.height;
            
            if(maxheight < screenHeight)
            {
                if(screenHeight > 480)
                    maxheight = 480;
                else
                    maxheight = 390;
                //maxheight = screenHeight * 1.05;
                if([arrmenuItems count] >= 3)
                    maxheight += ([arrmenuItems count]-3)*40;
            }
            _ScrollView.contentSize = CGSizeMake(320, maxheight);
            Basicimgbackground.frame = _ScrollView.frame;
            [loader removeFromSuperview];
        }
        
        if (webView == Webview)
        {
            [loader removeFromSuperview];
            if(webView.canGoBack)
            {
                NSString* strURL = [[Webview.request URL] absoluteString];
                int currTabTypeId = [[arrapptabtypeid objectAtIndex:z] intValue];
                
                if( [strURL hasPrefix:@"http://"] || [strURL hasPrefix:@"https://"] || [strURL isEqualToString:@"about:blank"])
                {
                    [backitem setHidden:NO];
                    backitem.enabled = YES;
                }
                else if ([strURL rangeOfString:@"index.html"].location != NSNotFound)
                {
                    if(lastTabTypeId == 12 || lastTabTypeId == 13)
                    {
                        [backitem setHidden:NO];
                        backitem.enabled = YES;
                    }
                    else
                    {
                        [backitem setHidden:YES];
                        backitem.enabled = NO;
                    }
                }
                else
                {
                    [backitem setHidden:NO];
                    backitem.enabled = YES;
                }
            }
            else
            {
                if(lastTabTypeId !=0 && ([[arrapptabtypeid objectAtIndex:z] intValue] == 12 || [[arrapptabtypeid objectAtIndex:z] intValue] == 13 ))
                {
                    [backitem setHidden:NO];
                    backitem.enabled = YES;
                }
                else
                {
                    lastTabTypeId = 0;
                    lastTabIndex = 0;
                    int icount = [arrMenuTabsNavigated count];
                    if(icount > 0)
                    {
                        lastTabIndex = [[arrMenuTabsNavigated objectAtIndex:icount-1] intValue];
                        lastTabTypeId = [[arrapptabtypeid objectAtIndex:lastTabIndex] intValue];
                    }
                    //Check current tabtype id
                    int currTabId = [[arrapptabid objectAtIndex:z] intValue];
                    if(
                       (currTabId != [[arrapptabid objectAtIndex:lastTabIndex] intValue])
                       &&
                       (lastTabTypeId == 12 || lastTabTypeId == 13)
                       )
                    {
                        [backitem setHidden:NO];
                        backitem.enabled = YES;
                    }
                    else
                    {
                        backitem.hidden = YES;
                        backitem.enabled = NO;
                    }
                }
            }
            
            
        }
        
        if (webView == DetailWebView) {
            fullScreenURL = [DetailWebView.request.mainDocumentURL absoluteString];
            [loader removeFromSuperview];
            
            if(isFullScreenMode == false)
            {
                backitem.hidden = YES;
                backitem.enabled = NO;
            }
            [self resizeDetailView];
        }
        if (webView == WebTopSquare)
        {
            CGRect frame = WebTopSquare.frame;
            
            frame.size.height = 1;
            WebTopSquare.frame = frame;
            CGSize fittingSize = [WebTopSquare sizeThatFits:CGSizeZero];
            frame.size = fittingSize;
            WebTopSquare.frame = frame;
            
            [_SquareView sendSubviewToBack:WebTopSquare];
            //[self.view sendSubviewToBack:_SquareView];
            //[self.view bringSubviewToFront:carousel1];
            
            NSLog(@"%f",frame.size.height);
            NSLog(@"%f  %f",carousel1.frame.size.height,carousel1.frame.origin.y);
        }
        
        if (webView == cameraFormWebView)
        {
            CGRect frame = cameraFormWebView.frame;
            
            frame.size.height = 1;
            cameraFormWebView.frame = frame;
            CGSize fittingSize = [cameraFormWebView sizeThatFits:CGSizeZero];
            frame.size = fittingSize;
            cameraFormWebView.frame = frame;
            
            cameraFormFieldsView.frame = CGRectMake(cameraFormFieldsView.frame.origin.x, frame.origin.y + frame.size.height, cameraFormFieldsView.frame.size.width, cameraFormFieldsView.frame.size.height);

            float viewH = camFormImageView.frame.size.height + cameraFormWebView.frame.size.height + cameraFormHeight + 550;
            //cameraFormFieldsView.frame = CGRectMake(0, fieldViewTop, 320, viewH);
            _ScrollView.contentSize = CGSizeMake(320, viewH);
        }
    }
    
    if([arrMenuTabsNavigated count] > 0)
    {
        [backitem setHidden:NO];
        backitem.enabled = YES;
        [loader removeFromSuperview];
    }
    if (webView==DetailWebView) {
        [loader removeFromSuperview];
    }
    [blackview removeFromSuperview];
}

-(void)resizeDetailView
{
    CGSize contentSize = DetailWebView.scrollView.contentSize;
    CGSize viewSize = DetailWebView.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    DetailWebView.scrollView.minimumZoomScale = rw;
    DetailWebView.scrollView.maximumZoomScale = rw;
    DetailWebView.scrollView.zoomScale = rw;
    
    DetailWebView.scrollView.contentOffset = CGPointMake(0, 0);
}

-(void)resizeBasicView
{
    
}

-(void)addEvent  :(NSString*)title :(NSString*)disc :(NSString*)dat
{
    EKEventStore *eventStore = [[[EKEventStore alloc] init]autorelease];
    if([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        [eventStore requestAccessToEntityType:EKEntityTypeEvent
                                completion:^(BOOL granted, NSError *err) {
                                    
                                    // may return on background thread
                                    dispatch_async(dispatch_get_main_queue(), ^{
                                        if (granted) {
                                            [self addEventToCalendar:tii :desc :ddd: eventStore];
                                        } else {
                                            UIAlertView *alert = [[[UIAlertView alloc]
                                                                  initWithTitle:@"Error"
                                                                  message:err.localizedDescription
                                                                  delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil]autorelease];
                                            [alert show];
                                            NSLog(@"%@",err.description);
                                        }
                                    });
                                }];    }
    else
    {
        [self addEventToCalendar:tii :desc :ddd: eventStore];
    }
}

- (void)addEventToCalendar  :(NSString*)title :(NSString*)disc :(NSString*)dat :(EKEventStore*) eventDB
{
    //EKEventStore *eventDB = [[EKEventStore alloc] init];
    EKEvent *myEvent  = [EKEvent eventWithEventStore:eventDB];
	myEvent.title     = [title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];;
    NSArray *arr123e = [dat componentsSeparatedByString:@"to"];
    NSString *str321 =[arr123e objectAtIndex:0];
    NSString *str123 = [str321 substringToIndex:str321.length];
    NSString *str234 = [[arr123e objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *daaa = [str123 stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    int interval = [daaa intValue];
    int endinterval = [str234 intValue];
    NSDate *datestart = [ NSDate dateWithTimeIntervalSince1970:interval];
    datestart = [self getLocalizedDate:datestart : interval];
    NSDate *dateend = [ NSDate dateWithTimeIntervalSince1970:endinterval];
    dateend = [self getLocalizedDate : dateend:endinterval];
    myEvent.startDate =  datestart;//[[NSDate alloc] init];
    myEvent.endDate   = dateend;//[[NSDate alloc] init];
    if (interval==endinterval)
    {
        myEvent.allDay = YES;
    }
	
    [myEvent setCalendar:[eventDB defaultCalendarForNewEvents]];
    NSError *err;
    
    [eventDB saveEvent:myEvent span:EKSpanThisEvent error:&err]; 
    
	if (err == nil) 
    {
		UIAlertView *alert = [[[UIAlertView alloc]
                              initWithTitle:@"Event Added Successfully"
                              message:@""
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil]autorelease];
		[alert show];
		
	}   
    else {
        UIAlertView *alert = [[[UIAlertView alloc]
                              initWithTitle:@"Error"
                              message:err.localizedDescription
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil]autorelease];
		[alert show];
        NSLog(@"%@",err.description);
    }

}

-(NSDate*) getLocalizedDate : (NSDate*) sourceDate :(int) dateInterval
{
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    int destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    int interval = dateInterval - destinationGMTOffset;
    
    NSDate* destinationDate = [ NSDate dateWithTimeIntervalSince1970:interval];
    
    return destinationDate;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    //arrtabhtml =[[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    isMainViewLoaded = false;
    
    CGRect ScreenSize = [ [ UIScreen mainScreen ] bounds ];
    heightFactor = 0;
    screenHeight = ScreenSize.size.height;
    screenWidth  = ScreenSize.size.width;
    
    topBarHeight = 44;
    tabBarHeight = 69;

    strUDID = [BPXLUUIDHandler UUID];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if( screenHeight > 480.0){
            heightFactor = screenHeight - 480;
        }
        isIPhone = true;
        isIPad = false;
    }
    else
    {
        isIPhone = false;
        isIPad = true;
    }
    
    mainBodyHeight = screenHeight - topBarHeight -tabBarHeight - heightFactor;
    
    carousel1.type = iCarouselTypeLinear;

    CGRect buttonRect = [backitem frame];
    buttonRect.origin.x = 7;
    backitem.frame = buttonRect;
    //Call loader
    //aaa=0;
    cb = 0;
    passtag = 211;
    if(!arraytags)
    {
    arraytags = [[NSMutableArray alloc]init];
    }
    if(!arrMenuTabsNavigated)
    {
        arrMenuTabsNavigated = [[NSMutableArray alloc]init];
    }
    if(!dictTabMenuItemsArray)
    {
    dictTabMenuItemsArray =[[NSMutableDictionary alloc]init];
    }
    if(!TAB_TYPES)
    {
    TAB_TYPES = [[NSMutableArray alloc] initWithObjects:@"/NOTAB",@"/Laddninssida",@"/Erbjudande",@"/FreeText",@"/Karta",@"/RSS",@"/Nyheter",@"/Gallery",@"/Kalender",@"/Form",@"/Link",@"/HTML(5)",@"/Basic Menu",@"/Square Menu",@"/Camera Form",  nil];
    }
    [self showLoader];
    
    // locationManager update as location
    if(!locationManager)
    {
    locationManager = [[CLLocationManager alloc] init];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
}

-(void) showLoader
{
    CGRect  rect = [[UIScreen mainScreen] bounds];
    
    float width = [[ UIScreen mainScreen ] bounds ].size.width;
    float height = [[ UIScreen mainScreen ] bounds ].size.height;
    
    float heightFactor = 0;
    if( height > 480.0){
        heightFactor = (height - 480)/2;
    }
    
    UIActivityIndicatorView *spinning = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]autorelease];
    
    if (check != 999)
    {
        bar.hidden = YES;
        tabbar.hidden = YES;
    }
    if (check == 999) 
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        {
            if(!loading)
            {
              loading  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 380)];
            }
        }
        else 
        {
            if(!loading)
            {
              loading  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
            }
        }
            loading.backgroundColor =[UIColor clearColor];
            loading.opaque = NO;
    }
    else 
    {
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *dataPath= [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
        NSString *ImagedataPath = [dataPath stringByAppendingPathComponent:@"Images"];
        NSString *resourcePath = @""; 
        resourcePath = [ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"welcome.png"]];
        UIImageView *imv;
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if(!loading)
            {
               loading  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            }
            
            imv = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, height)]autorelease];
            
            if(!blackview)
            {
               blackview = [[UIView alloc] initWithFrame:CGRectMake(rect.size.width/2 - 100 , rect.size.height/2 - 100, 200, 100)];
            }
            if(!prog)
            {
                prog = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
            }
            prog.frame = CGRectMake(blackview.frame.origin.x-50, blackview.frame.origin.y-85-heightFactor, 180, 40);//CGRectMake(rect.size.width/2 -100  , rect.size.height/2 + 120, 200, 10);
            lblpercent = [[UILabel alloc]initWithFrame:CGRectMake(blackview.frame.origin.x+70, blackview.frame.origin.y-75-heightFactor, 120, 28)];//CGRectMake(rect.size.width/2 -100  , rect.size.height/2 + 140, 300, 20)
            prog.progress = 0.0f;
            lblpercent.backgroundColor = [UIColor clearColor];
            lblpercent.text = [NSString stringWithFormat:@"0%%"];
            lblpercent.textColor = [UIColor whiteColor];
            //prog.hidden = YES;
            blackview.layer.cornerRadius = 20.0f;
            blackview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];//[self colorForHex:@"#636363"];255,0,0,0.5
            if(!loadLabel)
            {
            loadLabel = [[UILabel alloc] initWithFrame:CGRectMake(blackview.frame.origin.x-40, blackview.frame.origin.y-75-heightFactor, 120, 28)];
            }
            spinning.frame = CGRectMake(blackview.frame.size.width/2-18, blackview.frame.origin.y-130-heightFactor, 37, 37);

        }
        else 
        {
            width = 1024;
            height = 768;
            if(!loading)
            {
            loading  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            }
            imv = [[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, width, height)]autorelease];
            
            float frameX = width/2 - 200;
            float frameY = height/2 - 150;
            
            if(!blackview)
            {
            blackview = [[UIView alloc] initWithFrame:CGRectMake(frameX , frameY, 400, 150)];
            }
            if(!prog)
            {
            prog = [[UIProgressView alloc]initWithProgressViewStyle:UIProgressViewStyleDefault];
            }
            prog.frame = CGRectMake(frameX - 300, frameY - 150, 375, 40);
            if(!lblpercent)
            {
            lblpercent = [[UILabel alloc]initWithFrame:CGRectMake(frameX-70, frameY - 140, 128, 28)];
            }
            prog.progress = 0.0f;
            lblpercent.backgroundColor = [UIColor clearColor];
            lblpercent.text = [NSString stringWithFormat:@"0%%"];
            lblpercent.textColor = [UIColor whiteColor];
            //prog.hidden = YES;
            blackview.layer.cornerRadius = 20.0f;
            blackview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
            if(!loadLabel)
            {
               loadLabel = [[UILabel alloc] initWithFrame:CGRectMake(frameX-180, frameY - 140, 120, 28)];
            }
            spinning.frame = CGRectMake(frameX-130, frameY - 200,37, 37);
        }
        imv.image = [UIImage imageWithContentsOfFile:resourcePath];
        [loading  addSubview:imv];
        loading.opaque = NO;
    }
    
    loadLabel.text = @"Loading...";
    loadLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    loadLabel.textAlignment = UITextAlignmentCenter;
    
    if (check == 999) 
    {
        loadLabel.textColor = [UIColor whiteColor];
    }
    else 
    {
        loadLabel.textColor = [UIColor whiteColor];
    }
    
    loadLabel.backgroundColor = [UIColor clearColor];
    
    spinning.color = [UIColor whiteColor];

    [spinning startAnimating];
    [blackview addSubview:loadLabel];

    [blackview addSubview:spinning];
    [blackview addSubview:lblpercent];
    [blackview addSubview:prog];
    [loading addSubview:blackview];
    [self.view addSubview:loading];
}

- (void)checkstatus
{
    [self gettabshtmldata];
    ws = [[Webservice alloc]init];
    //[self loaddata];
}

- (BOOL) connectedToNetwork
{
    Reachability *rr =[Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [rr currentReachabilityStatus];
    BOOL internet;
	if ((internetStatus != ReachableViaWiFi) && (internetStatus != ReachableViaWWAN)) 
    {
		internet = NO;
 	} 
    else 
    {
		internet = YES;
  	}
    return internet;
} 

- (BOOL) checkInternet
{
	//Make sure we have internet connectivity
	if([self connectedToNetwork] != YES)
	{  
        return NO;
	}
	else 
    {
        return YES;        
	}
}

-(void)getzip
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSString* filePath = [NSString stringWithFormat:@"%@/temp.zip", dataPath];
    NSString* updateURL;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        updateURL = [NSString stringWithFormat:@"%@GetTabDetails.aspx?PlatformId=2&AppID=%@",serverURL,appid];
    }
    else 
    {
        updateURL = [NSString stringWithFormat:@"%@GetTabDetails.aspx?PlatformId=3&AppID=%@",serverURL,appid];
    }
    
    NSLog(@"Checking update at : %@", updateURL);
    NSString *content;
    NSData* updatedData = [NSData dataWithContentsOfURL: [NSURL URLWithString: updateURL]];
    NSFileManager *manager= [NSFileManager defaultManager];
    [manager createFileAtPath:filePath contents:updatedData attributes:nil];
    [SSZipArchive unzipFileAtPath:filePath toDestination:dataPath]; 
    if ([manager removeItemAtPath: filePath error: NULL]  == YES)
        NSLog (@"Remove successful : %@",filePath);  
    NSString *xmlfilepath =[dataPath stringByAppendingPathComponent:@"XML"];
    NSString *filedataPath;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        filedataPath = [xmlfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"Default_%@_2.txt",appid]];
    }
    else 
    {
        filedataPath = [xmlfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"Default_%@_3.txt",appid]];
    }
    content = [NSString stringWithContentsOfFile:filedataPath encoding:NSUTF8StringEncoding error:NULL];
    
    [self jsondat:content];
}


- (NSString*)getsaveddata
{
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
    NSString *xmlfilepath =[dataPath stringByAppendingPathComponent:@"XML"];
    NSString *filedataPath;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        filedataPath = [xmlfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"Default_%@_2.txt",appid]];
    }
    else 
    {
        filedataPath = [xmlfilepath stringByAppendingPathComponent:[NSString stringWithFormat:@"Default_%@_3.txt",appid]];
    }
    NSString* content = [NSString stringWithContentsOfFile:filedataPath encoding:NSUTF8StringEncoding error:NULL];
    if (content != nil) 
    {
        [self jsondat:content];
    }
    
    return content;
}

- (void)jsondat:(NSString*)data
{
    NSLog(@"Tab content:\n%@",data);
    arr = [data JSONValue];
    appname = [[NSMutableArray alloc]init];
    arrapptabtypeid = [[NSMutableArray alloc]init];
    if(!arrtabnames)
    {
    arrtabnames =[[NSMutableArray alloc]init];
    }
    arrtabicons =[[NSMutableArray alloc]init];
    arrtabid =[[NSMutableArray alloc]init];
    arrurls =[[NSMutableArray alloc]init];
    arrheader = [[NSMutableArray alloc]init];
    arrpassword =[[NSMutableArray alloc]init];
    arrButtonBackgroungdImage = [[NSMutableArray alloc]init];
    arrButtonBackgroungdImageipad = [[NSMutableArray alloc]init];
    arrButtonBackgroundColoripad = [[NSMutableArray alloc]init];
    arrHeaderBackgroungdImage = [[NSMutableArray alloc]init];
    arrHeaderBackgroungdImageipad = [[NSMutableArray alloc]init];
    arrHeaderBackgroungcolor = [[NSMutableArray alloc]init];
    arrHeaderBackgroungcoloripad = [[NSMutableArray alloc]init];
    arrButtonBackgroundColor = [[NSMutableArray alloc]init];
    arrButtonTextcolor =[[NSMutableArray alloc]init];
    arrHeaderTextcolor = [[NSMutableArray alloc]init];
    arrHeaderFontStyle = [[NSMutableArray alloc]init];
    arrheaderfonttype = [[NSMutableArray alloc]init];
    arrbuttonFontStyle = [[NSMutableArray alloc]init];
    arrpublisheddate = [[NSMutableArray alloc]init];
    arrheaderfontsize = [[NSMutableArray alloc]init];
    arrbuttonfontsize = [[NSMutableArray alloc]init];
    arrtabsize = [[NSMutableArray alloc]init];
    arrtabhide = [[NSMutableArray alloc]init];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath= [documentsDirectory stringByAppendingPathComponent:@"MyFolder"];
    NSFileManager *manager= [NSFileManager defaultManager];
    NSString *ImagedataPath = [dataPath stringByAppendingPathComponent:@"Images"];
    dict =[[NSMutableDictionary alloc]init];
    currentTabs =[[NSMutableDictionary alloc]init];
    if(!arrVisibleTabs)
    {
    arrVisibleTabs = [[NSMutableArray alloc]init];
    }
    
    if(!dictSquareMenuSelectedItems)
    {
    dictSquareMenuSelectedItems =[[NSMutableDictionary alloc]init];
    }
    
    for (int i=0; i<[arr count]; i++)
    {
        dict = [arr objectAtIndex:i];
        NSString *str1 =[[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"TabName"]]autorelease];
        NSString *strurl =[[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"URL"]]autorelease];
        NSString *icon =[[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"Icon"]]autorelease];
        NSString *strtabtypeid = [[[NSString alloc]initWithFormat:@"%d",[[dict valueForKey:@"TabTypeID"] intValue]]autorelease];
        NSString *strtabHide = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"IsHide"]]autorelease];
        UIImage *imgicon;
        if ([strtabtypeid intValue] != 1)
        {
            if ([icon isEqualToString:@""])
            {
                imgicon = [UIImage imageNamed:@"images copy.png"];
                [arrtabicons addObject:imgicon];
            }
            else
            {
                NSString *strappname = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"AppName"]]autorelease];
                NSString *strtabid =[[[NSString alloc]initWithFormat:@"%d",[[dict valueForKey:@"AppTabId"] intValue]]autorelease];
                NSString *strapptabtypeid = [[[NSString alloc]initWithFormat:@"%d",[[dict valueForKey:@"TabTypeID"] intValue]]autorelease];
                NSString *strheaderbgi =[[[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"HeaderBackgroungdImageiPhone"]]autorelease];
                NSString *strbuttonbgi = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonBackgroungdImageiPhone"]]autorelease];
                NSString *strheaderbgiipad =[[[NSString alloc] initWithFormat:@"%@",[dict valueForKey:@"HeaderBackgroungdImageiPad"]]autorelease];
                NSString *strbuttonbgiipad = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonBackgroungdImageiPad"]]autorelease];
                NSString *ImagePath = [[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",icon]]autorelease];
                imgicon =[UIImage imageWithContentsOfFile:ImagePath];
                if (imgicon == nil)
                {
                    imgicon = [UIImage imageNamed:@"defaulttabicon.png"];
                }
                
                NSString *strheadertxtcolor = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"HeaderTextcolor"]]autorelease];
                NSString *strbtntxtcolor = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonTextcolor"]]autorelease];
                NSString *strheaderfontstyle = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"HeaderFontStyle"]]autorelease];
                NSString *strbuttonfontstyle = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonFontStyle"]]autorelease];
                NSString *strbuttonbackcolor = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonBackgroundColor"]]autorelease];
                NSString *strheaderbgcolor = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"HeaderBackgroundColor"]]autorelease];
                NSString  *strfilesize = [[[NSString alloc]initWithFormat:@"%d",[[dict valueForKey:@"FileSize"] intValue]]autorelease];
                
                NSString *strheaderfontsize;
                if ([dict valueForKey:@"HeaderFontSize"] == [NSNull null])
                {
                    strheaderfontsize = @"20";
                }
                else
                {
                    strheaderfontsize = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"HeaderFontSize"]]autorelease];
                }
                NSString *strbuttonfontsize;
                if ([dict valueForKey:@"ButtonFontSize"]== [NSNull null])
                {
                    strbuttonfontsize = @"12";
                }
                else
                {
                    strbuttonfontsize = [[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"ButtonFontSize"]]autorelease];
                }
                NSString *strdate =[[[NSString alloc]initWithFormat:@"%@",[dict valueForKey:@"LastPublishDate"]]autorelease];
                
                BOOL p = [[dict valueForKey:@"PasswordProtected"] boolValue];
                
                [arrpassword addObject:[NSNumber numberWithBool:p]];
                [appname addObject:strappname];
                [arrapptabtypeid addObject:strapptabtypeid];
                [arrtabnames addObject:str1];
                [arrtabicons addObject:imgicon];
                [arrtabid addObject:strtabid];
                [arrurls addObject:strurl];
                [arrHeaderBackgroungdImage addObject:strheaderbgi];
                [arrButtonBackgroungdImage addObject:strbuttonbgi];
                [arrHeaderBackgroungdImageipad addObject:strheaderbgiipad];
                [arrButtonBackgroungdImageipad addObject:strbuttonbgiipad];
                [arrHeaderBackgroungcolor addObject:strheaderbgcolor];
                [arrButtonBackgroundColor addObject:strbuttonbackcolor];
                [arrHeaderTextcolor addObject:strheadertxtcolor];
                [arrButtonTextcolor addObject:strbtntxtcolor];
                [arrHeaderFontStyle addObject:strheaderfontstyle];
                [arrbuttonFontStyle addObject:strbuttonfontstyle];
                [arrheaderfontsize addObject:strheaderfontsize];
                [arrbuttonfontsize addObject:strbuttonfontsize];
                [arrpublisheddate addObject:strdate];;
                [arrtabsize addObject:strfilesize];
                [arrtabhide addObject:strtabHide];
                
                if(![strtabHide boolValue])
                {
                    [arrVisibleTabs addObject:[NSNumber numberWithInteger:(i-1)]];
                }
                
                totalTabSize += [strfilesize intValue];
                
                NSDateFormatter *inputFormatter = [[[NSDateFormatter alloc] init] autorelease];
                inputFormatter.locale = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease];
                [inputFormatter setDateFormat:@"MM/dd/yyyy hh:mm:ss a"];
                NSDate *pubDate = [inputFormatter dateFromString:strdate];
                
                if (flag == 222)
                {
                    [oldTabs setObject:pubDate forKey:strtabid];
                }
                else
                {
                    [currentTabs setObject:pubDate forKey:strtabid];
                }
            }
        }
        if ([strtabtypeid intValue] == 1)
        {
            
            apppassword = [[dict valueForKey:@"PasswordProtected"] boolValue];
            NSLog(@"%x",apppassword);
            WelcomeImagePath = [ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",icon]];
            NSString *resourcePath = @"";
            resourcePath = [ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",@"welcome.png"]];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:resourcePath];
            if(fileExists)
            {
                [manager removeItemAtPath:resourcePath error: NULL];
            }
            if(![icon isEqualToString:@""])
            {
                [manager copyItemAtPath:WelcomeImagePath toPath:resourcePath error:nil];
            }
            [loading removeFromSuperview];
            [self performSelectorInBackground:@selector(showLoader) withObject:nil];
            
        }
    }
    arrtabs= arrtabnames;
    arrtabimgs = arrtabicons;
    arrapptabid = arrtabid;
    arrtaburls =arrurls;
    arrpasswordstatus = arrpassword;
    NSLog(@"%@",arrtabnames );
    NSLog(@"%@",arrapptabtypeid);
    NSLog(@"%@",arrVisibleTabs);
}

- (void) showWelcomeImage:(NSString*)welcomeImagePath
{
    UIColor *color = [[[UIColor alloc] initWithPatternImage:[UIImage imageWithContentsOfFile:welcomeImagePath]]autorelease];
    loading.backgroundColor = color;
}

-(void)gettabshtmldata
{
    if(!arrtabhtml)
    {
    arrtabhtml =[[NSMutableArray alloc]init];
    }
    if(!arrhtmlpaths)
    {
    arrhtmlpaths =[[NSMutableArray alloc]init];
    }
    if(!arrpublisheddate)
    {
    arrpublisheddate =[[NSMutableArray alloc]init];
    }
    if(!arrdates)
    {
    arrdates =[[NSMutableArray alloc]init];
    }
    [dictTabMenuItemsArray removeAllObjects];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
    if([self connectedToNetwork]) 
    {
        //Read older tab data
        flag = 222;
        oldTabs = [[NSMutableDictionary alloc]init];
        [self getsaveddata];
        //Get new tab data
        flag = 111;
        [self getzip];
        if (apppassword ==YES)
        {
            [self showalert];
            loading = nil;
            return;
        }

    }
    else
    {
        oldTabs = [[NSMutableDictionary alloc]init];
        flag = 111;
        [self getsaveddata];
    }
    bool anyUpdatesFromServer = false;    
    for (int i=0; i<[arrapptabid count]; i++)
    {
        NSString *tabdataPath;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%@",appid,[arrapptabid objectAtIndex:i]]];
        }
        else
        {
            tabdataPath  = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%@",appid,[arrapptabid objectAtIndex:i]]];
        }
        
        NSString *tabhtmlpath = [tabdataPath stringByAppendingPathComponent:@"index.html"];
        [arrhtmlpaths addObject:tabhtmlpath];
        
        if([[arrapptabtypeid objectAtIndex:i] intValue] != 5)  //Ignore RSS
        {
            long oldTick = 0;
            if ([oldTabs objectForKey:[arrapptabid objectAtIndex:i]] != nil ) 
            {
                oldTick = [[oldTabs objectForKey:[arrapptabid objectAtIndex:i]] timeIntervalSince1970];
            }
            long newTick = 0;
            if ([currentTabs objectForKey:[arrapptabid objectAtIndex:i]] != nil ) 
            {
                newTick = [[currentTabs objectForKey:[arrapptabid objectAtIndex:i]] timeIntervalSince1970];
            }
            //Check if currentTabId exists in older tab data
            if (newTick > oldTick ) 
            {
                anyUpdatesFromServer = true;
                [self downloadDataForTab:appid :[arrtabid objectAtIndex:i]:i];
            }
        }
        
    }

    if(anyUpdatesFromServer == false)
    {
        [self loaddata];
    }
}

-(void)downloadDataForTabSynchronized:(NSString*) appid:(NSString*) appTabID
{
    NSFileManager *manager= [NSFileManager defaultManager];
     NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSString* tabfilePath = [NSString stringWithFormat:@"%@/tabdetails%@.zip", dataPath,appTabID];
    NSString* updateURL;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        updateURL  =[NSString stringWithFormat:@"%@GetHTMLData.aspx?AppTabId=%@&AppId=%@&PlatformID=2&Version=%@",serverURL,appTabID,appid,[UIDevice currentDevice].systemVersion];
    }
    else
    {
        updateURL  =[NSString stringWithFormat:@"%@GetHTMLData.aspx?AppTabId=%@&AppId=%@&PlatformID=3&Version=%@",serverURL,appTabID,appid,[UIDevice currentDevice].systemVersion];
    }
    
    NSData* updatedData = [NSData dataWithContentsOfURL: [NSURL URLWithString: updateURL] ];
    [manager createFileAtPath:tabfilePath contents:updatedData attributes:nil];
    [SSZipArchive unzipFileAtPath:tabfilePath toDestination:dataPath];
    if ([manager removeItemAtPath: tabfilePath error: NULL]  == YES)
        NSLog (@"Remove successful : %@",tabfilePath);
}

-(void)downloadDataForTab:(NSString*) appid:(NSString*) appTabID :(int)a
{
    NSFileManager *manager= [NSFileManager defaultManager];
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSString* tabfilePath = [NSString stringWithFormat:@"%@/tabdetails%@.zip", dataPath,appTabID];
    NSString* updateURL;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        updateURL  =[NSString stringWithFormat:@"%@GetHTMLData.aspx?AppTabId=%@&AppId=%@&PlatformID=2&Version=%@",serverURL,appTabID,appid,[UIDevice currentDevice].systemVersion];
    }
    else
    {
        updateURL  =[NSString stringWithFormat:@"%@GetHTMLData.aspx?AppTabId=%@&AppId=%@&PlatformID=3&Version=%@",serverURL,appTabID,appid,[UIDevice currentDevice].systemVersion];
    }
    
    NSLog(@"Download: %@",updateURL);
    
    NSURL *URL = [NSURL URLWithString:updateURL];
    NSURLCacheStoragePolicy policy =NSURLRequestUseProtocolCachePolicy;// NSURLCacheStorageNotAllowed;
    NSURLRequest *request = [NSURLRequest requestWithURL:URL cachePolicy:policy timeoutInterval:100];
    RQOperation *operation = [RQOperation operationWithRequest:request];
    
    operation.completionHandler = ^(NSURLResponse *response, NSData *data, NSError *error)
    {
        if (!error)
        {
            //Data downloaded
            NSLog(@"response   %@   %d",response,data.length);
            [manager createFileAtPath:tabfilePath contents:data attributes:nil];
            [SSZipArchive unzipFileAtPath:tabfilePath toDestination:dataPath];
            if ([manager removeItemAtPath: tabfilePath error: NULL]  == YES)
                NSLog (@"Remove successful : %@",tabfilePath);
        }
        else {
            NSLog(@"error   %@",error);
        }
        if([[RequestQueue mainQueue] requestCount] == 0)
        {
            //UIAlertView *alt =[[UIAlertView alloc]initWithTitle:@"All requests completed" message:@"" delegate:self cancelButtonTitle:@"ok" otherButtonTitles: nil];
            //[alt show];
            [self updateProgress:1];
            [self getcommondata];
        }
        else
        {
            NSLog(@"Pending requests::  %i",[[RequestQueue mainQueue] requestCount]);
        }
        
    };
    
    operation.downloadProgressHandler = ^(float progress, NSInteger bytesTransferred, NSInteger totalBytes)
    {
        
        //update progress
        float percentProgress = bytesTransferred + currentDownloadedBytes;
        percentProgress = percentProgress / totalTabSize;
        [self updateProgress:percentProgress];
        //NSLog(@"Progress %f",percentProgress);
        if(bytesTransferred == totalBytes)
        {
            currentDownloadedBytes = currentDownloadedBytes + totalBytes;
            NSLog(@"Total downloaded: %i of %i = %f",currentDownloadedBytes, totalTabSize, ((float)currentDownloadedBytes/totalTabSize));
            
        }
    };
    
    //add operation to queue
    [[RequestQueue mainQueue] addOperation:operation];
}


-(void)ggg :(RQOperation*)operation
{
    [[RequestQueue mainQueue] addOperation:operation];
}

- (void)updateProgress:(float)percentProgress
{
    if(percentProgress > [prog progress])
    {
        prog.hidden = NO;
        [prog setProgress:percentProgress animated:YES];
        float aa = percentProgress;
        aa= aa*100;
        int a  = aa;
        //NSLog(@"Progress %i",a);
        lblpercent.text = [NSString stringWithFormat:@"%d%%",a];
        lblpercent.textColor = [UIColor whiteColor];
    }
}

-(void)getcommondata
{
    
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"TabFolder"];
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    NSFileManager *manager= [NSFileManager defaultManager];
    NSString* filePath = [NSString stringWithFormat:@"%@/Common.zip", dataPath];
    NSString* filePath1233 = [NSString stringWithFormat:@"%@/Common", dataPath];
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath1233])
    {
        NSString* updateURL = [NSString stringWithFormat:@"%@/Common.zip", serverURL];
        NSData* updatedData = [NSData dataWithContentsOfURL: [NSURL URLWithString: updateURL] ];
        [manager createFileAtPath:filePath contents:updatedData attributes:nil];
        [SSZipArchive unzipFileAtPath:filePath toDestination:dataPath];
        if ([manager removeItemAtPath: filePath error: NULL]  == YES)
            NSLog (@"Remove successful : %@",filePath);  
    }
    [self performSelector:@selector(loaddata)];
}

/*- (UIImage *)selectionIndicatorImage
{
    NSUInteger count    = [[self.tabBarController viewControllers] count];
    CGSize tabBarSize = [[self.tabBarController tabBar] frame].size;
    NSUInteger padding = 2;
    
    CGSize buttonSize = CGSizeMake( tabBarSize.width / count, tabBarSize.height );
    
    UIGraphicsBeginImageContext( buttonSize );
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    [[UIColor colorWithWhite:0.9 alpha:0.1] setFill];
    
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:CGRectMake( padding, padding * 2, buttonSize.width - (padding * 2) , buttonSize.height - ( padding * 2 ) ) cornerRadius:4.0];
    
    [roundedRect fillWithBlendMode: kCGBlendModeNormal alpha:1.0f];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    CGContextRelease( c );
    
    return image;
}*/

-(void)loaddata
{
    //tabbar.frame=
    // an =YES;  // no use in code 
    flag = 0; 
    [self.view bringSubviewToFront:tabbar];
    if(loading != nil )
    {
        [loading removeFromSuperview];
    }
    bar.hidden = NO;
    tabbar.hidden = NO;
    act.hidden = YES;
    appDelegate =(AppDelegate*)[UIApplication sharedApplication].delegate;
    if(!we)
    {
      we  = [[Webservice alloc]init];
    }
    if(!ARRNAMES)
    {
      ARRNAMES = [[NSMutableArray alloc]init];
    }
    if(!ARRIMAGES)
    {
      ARRIMAGES = [[NSMutableArray alloc]init];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
    {
        for (int i = 4; i<[arrVisibleTabs count]; i++) 
        {
            [ARRNAMES addObject:[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:i]intValue]]];
            [ARRIMAGES addObject:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:i]intValue]]];
        }
    }
    else 
    {
        for (int i = 9; i<[arrVisibleTabs count]; i++)
        {
            [ARRNAMES addObject:[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:i]intValue]]];
            [ARRIMAGES addObject:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:i]intValue]]];
        }
    }
    
    [bar setHidden:YES];
    [backitem setHidden:YES];
    [backitem addTarget:self action:@selector(reloadwebview:) forControlEvents:UIControlEventTouchUpInside];
    if(!tab6)
    {
       tab6 = [[UITabBarItem alloc]initWithTabBarSystemItem:UITabBarSystemItemMore tag:100];
    }
    if ([we callWebService:[appid intValue] :[appid intValue] :textfieldName.text :textfieldPassword.text:serverURL]==0)
    {
        NSLog(@"ok");
    }
    
    else 
    {
        UIAlertView *alt = [[[UIAlertView alloc]initWithTitle:@"Incorrect Credentials" message:@"Enter Valid Userid and Password" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil]autorelease];
        [alt show];
    }
    
    if ([arrVisibleTabs count]>0)
    {
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]] substringToIndex:8];
            tab1.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            tab1.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]];
        }
        NSLog(@"%@",[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]]);
        if ([UIScreen mainScreen].scale ==2.0) 
        {
            UIImage *IMM =[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]];
            CGSize size = IMM.size;
            NSLog(@"%f  %f",size.height,size.width);
            tab1.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]];
            NSLog(@"Retina display");
        }
        else 
        {
            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:0]intValue]] newSize:CGSizeMake(30, 30)];
            NSLog(@"non-Retina display");
            tab1.image =imgaaaa;
        } 
    }
    else
    {
        tab1.enabled = NO;
    }
    
    if ([arrVisibleTabs count] > 1)
    {
        if(!tab2)
        {
        tab2 = [[UITabBarItem alloc]init];
        }
        //tab2 = [[UITabBarItem alloc]initWithTitle:@"Free Text" image:[self selectionIndicatorImage] tag:122];
        tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2, nil];
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:1]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:1]intValue]] substringToIndex:8];
            tab2.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            tab2.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:1]intValue]];
        }
        if ([UIScreen mainScreen].scale ==2.0)
        {
            tab2.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:1]intValue]];
            NSLog(@"Retina display");
        }
        else 
        {
            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:1]intValue]] newSize:CGSizeMake(30, 30)];
            NSLog(@"non-Retina display");
            tab2.image =imgaaaa;
        }
    }
    else
    {
        tab2.enabled = NO;
    }
    
    if ([arrVisibleTabs count]>2)
    {
        if(!tab3)
        {
           tab3 = [[UITabBarItem alloc]init];
        }
        tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3, nil];
        
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:2]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:2]intValue]] substringToIndex:8];
            tab3.title = [NSString stringWithFormat:@"%@...",firstFive];
        }
        else
        {
            tab3.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:2]intValue]];
        }
        if ([UIScreen mainScreen].scale == 2.0) 
        {
            tab3.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:2]intValue]];
            NSLog(@"Retina display");
        }
        else 
        {
            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:2]intValue]] newSize:CGSizeMake(30, 30)];
            NSLog(@"non-Retina display");
            tab3.image =imgaaaa;
        }
    }
    else
    {
        tab3.enabled = NO;
    }
    
    if ([arrVisibleTabs count]>3)
    {
        if(!tab4)
        {
        tab4 = [[UITabBarItem alloc]init];
        }
        tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4, nil];
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:3]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:3]intValue]] substringToIndex:8];
            tab4.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            tab4.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:3]intValue]];
        }
        if ([UIScreen mainScreen].scale ==2.0) 
        {
            tab4.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:3]intValue]];
            NSLog(@"Retina display");
        }
        else 
        {
            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:3]intValue]] newSize:CGSizeMake(30, 30)];
            NSLog(@"non-Retina display");
            tab4.image =imgaaaa;
        }
    }
    else
    {
        tab4.enabled = NO;
    }
    
    if ([arrVisibleTabs count]>4)
    {
        if(!tab5)
        {
          tab5 = [[UITabBarItem alloc]init];
        }
        tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5, nil];
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:4]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:4]intValue]] substringToIndex:8];
            tab5.title = [NSString stringWithFormat:@"%@...",firstFive] ;}
        else
        {
            
            tab5.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:4]intValue]];
        }
        
    }
    else
    {
        tab5.enabled = NO;
    }
    
    
    if ([arrVisibleTabs count]>5)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab6, nil];
        }
        else 
        {
            if(!tab7)
            {
             tab7 =  [[UITabBarItem alloc]init];
            }
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5,tab7, nil];
        }
        
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:5]intValue]] length] > 8) 
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:5]intValue]] substringToIndex:8];
            tab7.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            
            tab7.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:5]intValue]];
        }
    }
    else{
        tab6.enabled = NO;
        tab6.image = nil;
        tab6.title = @"";
        tab7.enabled = NO;
    }  
    if ([arrVisibleTabs count]>6)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab6, nil];
        }
        else 
        {
            if(!tab8)
            {
            tab8 =  [[UITabBarItem alloc]init];
            }
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5,tab7,tab8, nil];
        }

        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:6]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:6]intValue]] substringToIndex:8];
            tab8.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            
            tab8.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:6]intValue]];
        }
    }
    else
    {
        tab8.enabled = NO;
    }  
    if ([arrVisibleTabs count]>7)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab6, nil];
        }
        else 
        {
            if(!tab9)
            {
            tab9 =  [[UITabBarItem alloc]init];
            }
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5,tab7,tab8,tab9, nil];
        }
        
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:7]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:7]intValue]] substringToIndex:8];
            tab9.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            tab9.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:7]intValue]];
        }
    }
    else{
        tab9.enabled = NO;
    }  
    if ([arrVisibleTabs count]>8)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) 
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab6, nil];
        }
        else 
        {
            if(!tab10)
            {
            tab10 =  [[UITabBarItem alloc]init];
            }
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5,tab7,tab8,tab9,tab10, nil];
        }
        if ([[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:8]intValue]] length] > 8)
        {
            firstFive = [[arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:8]intValue]] substringToIndex:8];
            tab10.title = [NSString stringWithFormat:@"%@...",firstFive] ;
        }
        else
        {
            tab10.title = [arrtabnames objectAtIndex:[[arrVisibleTabs objectAtIndex:8]intValue]];
        }

    }
    else
    {
       tab10.enabled = NO;
    }  
    if ([arrVisibleTabs count]>9)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab6, nil];
        }
        else
        {
            tabbar.items = [[NSArray alloc] initWithObjects:tab1,tab2,tab3,tab4,tab5,tab7,tab8,tab9,tab10,tab6, nil];
        }
        
    }
    else
    {
        // tab9.enabled = NO;
    }

    if ([arrVisibleTabs count]>4) 
    {
        if ([UIScreen mainScreen].scale ==2.0) 
        {
            tab5.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:4]intValue]];
            NSLog(@"Retina display");
        }
        else 
        {
            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:4]intValue]] newSize:CGSizeMake(30, 30)];
            NSLog(@"non-Retina display");
            tab5.image =imgaaaa;
        }
        if ([arrVisibleTabs count]>5)
        {
            if ([UIScreen mainScreen].scale ==2.0) 
            {
                tab7.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:5]intValue]];
                NSLog(@"Retina display");
            }
            else 
            {
                UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:5]intValue]] newSize:CGSizeMake(30, 30)];
                NSLog(@"non-Retina display");
                tab7.image =imgaaaa;
            }
            if ([arrVisibleTabs count]>6)
            {
                if ([UIScreen mainScreen].scale ==2.0) 
                {
                    tab8.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:6]intValue]];
                    NSLog(@"Retina display");
                }
                else 
                {
                    UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:6]intValue]] newSize:CGSizeMake(30, 30)];
                    NSLog(@"non-Retina display");
                    tab8.image =imgaaaa;
                }
                if ([arrVisibleTabs count]>7)
                {
                    if ([UIScreen mainScreen].scale ==2.0) 
                    {
                        tab9.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:7]intValue]];
                        NSLog(@"Retina display");
                    }
                    else 
                    {
                        UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:7]intValue]] newSize:CGSizeMake(30, 30)];
                        NSLog(@"non-Retina display");
                        tab9.image =imgaaaa;
                    }
                    if ([arrVisibleTabs count]>8)
                    {
                        if ([UIScreen mainScreen].scale ==2.0)
                        {
                            tab10.image = [arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:8]intValue]];
                            NSLog(@"Retina display");
                        }
                        else
                        {
                            UIImage *imgaaaa = [self resizeImage:[arrtabimgs objectAtIndex:[[arrVisibleTabs objectAtIndex:8]intValue]] newSize:CGSizeMake(30, 30)];
                            NSLog(@"non-Retina display");
                            tab10.image =imgaaaa;
                        }
                    }

                }
            }
        }
    }
    
    tab1.tag = 0;
    tab2.tag = 1;
    tab3.tag = 2;
    tab4.tag = 3;
    tab5.tag = 4;
    tab7.tag = 5;
    tab8.tag = 6;
    tab9.tag = 7;
    tab10.tag = 8;
    
    [tabbar setSelectedItem:tab1];
    [self tabBar:tabbar didSelectItem:tab1];

}

- (UIImage *)resizeImage:(UIImage*)image newSize:(CGSize)newSize
{
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGImageRef imageRef = image.CGImage;
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    CGContextConcatCTM(context, flipVertical);  
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();    
    
    return newImage;
}

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height;
    float scaleFactor = i_height / oldHeight;
    
    float newWidth = sourceImage.size.width * scaleFactor;
    float newHeight = oldHeight * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
    latitude = [[NSString alloc]initWithFormat:@"%g",newLocation.coordinate.latitude];
    longitude = [[NSString alloc]initWithFormat:@"%g",newLocation.coordinate.longitude];
    coordinate = newLocation.coordinate;
    [locationManager stopUpdatingLocation];
    CLGeocoder * geoCoder = [[[CLGeocoder alloc] init]autorelease];
    [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) 
    {
        for (CLPlacemark * placemark in placemarks) 
        {
            strlocation = [placemark locality];
        }    
    }];
    
    self.currentLocation = newLocation;    
    coordinate = [self.currentLocation coordinate];
    latitude = [NSString stringWithFormat:@"%f", coordinate.latitude]; 
    longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
//    if(newLocation.horizontalAccuracy <= 100.0f)
//    { 
//        [locationManager stopUpdatingLocation]; 
//    }
    
    NSLog(@"latitude is %@",latitude);
    NSLog(@"longitude is %@",longitude);
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error 
{
    NSLog(@"locationManager:%@ didFailWithError:%@", manager, error);
}

- (void)reverseGeocoder:(CLGeocoder *)geocoder didFailWithError:(NSError *)error
{
    NSLog(@"reverseGeocoder:%@ didFailWithError:%@", geocoder, error);
}

- (UIColor *) colorForHex:(NSString *)hexColor : (CGFloat) opacity
{
    hexColor = [[hexColor stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceAndNewlineCharacterSet]
                 ] uppercaseString];
    
    // String should be 6 or 7 characters if it includes ‘#’
    if ([hexColor length] < 6)
        return [UIColor blackColor];
    
    // strip # if it appears
    if ([hexColor hasPrefix:@"#"])
        hexColor = [hexColor substringFromIndex:1];
    
    // if the value isn’t 6 characters at this point return
    // the color black
    if ([hexColor length] != 6)
        return [UIColor blackColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    NSString *rString = [hexColor substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [hexColor substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [hexColor substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)  green:((float) g / 255.0f)  blue:((float) b / 255.0f)  alpha:opacity];
    
}

-(UIColor *) colorForHex:(NSString *)hexColor
{ 
 
    return [self colorForHex:hexColor:1.0f];
    
}
#pragma  mark Tableview datasource and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView ==table)
    {
        // Return the number of rows in the section.
        return [ARRNAMES count];
    }
    else //if(tableView == basicMenuTable)
    {
        NSLog(@"%@",arrmenuItems);
        return [arrmenuItems count]-1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UIImageView *imggg;
    UnderLineLabel *lable;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) 
    {
        
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
        if (tableView ==basicMenuTable) {
            imggg = [[[UIImageView alloc] initWithFrame:CGRectZero]autorelease];
            imggg.tag = 1;
            
            lable = [[[UnderLineLabel alloc] initWithFrame:CGRectMake(0.0, 0.0, cell.frame.size.width, cell.frame.size.height)]autorelease];

           lable.backgroundColor = [UIColor clearColor];
            lable.tag = 2;
            lable.font = [UIFont systemFontOfSize:14.0];
            UIView *message = [[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, cell.frame.size.width, cell.frame.size.height)]autorelease];
            message.tag = 0;
            [message addSubview:imggg];
            [message addSubview:lable];
            [cell.contentView addSubview:message];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
        
    }
    else
    {
        if (tableView == basicMenuTable) {
            imggg = (UIImageView *)[[cell.contentView viewWithTag:0] viewWithTag:1];
            lable = (UnderLineLabel *)[[cell.contentView viewWithTag:0] viewWithTag:2];
        }
       
        
    }
    

      if (tableView ==table) {
        // Configure the cell...
        cell.textLabel.text =[ARRNAMES objectAtIndex:indexPath.row];
        cell.imageView.image =[ARRIMAGES objectAtIndex:indexPath.row];
    }
    
    else //if (tableView ==basicMenuTable) {
    {
        NSMutableDictionary *menuItemDict11 = [arrmenuItems objectAtIndex:0];
        NSString* Fontsize = [self getValueFromDictionary:menuItemDict11:@"FontSize"];
        
        NSString* FontStyle = [self getValueFromDictionary:menuItemDict11:@"FontStyle"];
        
        NSString* FontStyleType = [self getValueFromDictionary:menuItemDict11:@"FontStyleType"];
        NSString* Textcolor = [self getValueFromDictionary:menuItemDict11:@"Textcolor"];
        
        NSMutableDictionary *menuItemDict = [arrmenuItems objectAtIndex:indexPath.row + 1 ];
        NSString* menuItemName = [self getValueFromDictionary:menuItemDict:@"MenuItemName"];
        NSString* menuItemBgColor = [self getValueFromDictionary:menuItemDict:@"FieldColor"];
        NSString* menuItemBgIphoneImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIphoneImage"];
        NSString* menuItemBgIpadImage = [self getValueFromDictionary:menuItemDict:@"BackGroundIpadImage"];
        NSString* menuItemArrowImage = [self getValueFromDictionary:menuItemDict:@"Arrowcolor"];
        NSString* opacityStr = [self getValueFromDictionary:menuItemDict:@"Opacity"];
        
        CGFloat menuItemOpacity = 1.0f;
        if(opacityStr != nil )
        {
            menuItemOpacity = [opacityStr floatValue] / 100.0f;
        }
        
        NSString *imgpath,*imgArrowpath;
        NSString *ImagedataPath = [self getTabFolderBasePath];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            if([menuItemBgIphoneImage isEqualToString:@""]) //Use bgColor
            {
                UIColor *bgColor = [self colorForHex:menuItemBgColor:menuItemOpacity];
                NSLog(@"bgColor %@",bgColor);
                imggg.backgroundColor = bgColor;
                cell.backgroundColor = bgColor;
                //lable.backgroundColor = bgColor ;//[UIColor clearColor];
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                CGRect frame = CGRectMake(0.0, 0.0, 14, 14);
                button.frame = frame;
                 imgArrowpath =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemArrowImage]];
                [button setBackgroundImage:[UIImage imageWithContentsOfFile:imgArrowpath] forState:UIControlStateNormal];
                button.backgroundColor = [UIColor clearColor];
                cell.accessoryView = button;

            }
            else
            {
                cell.backgroundColor = [UIColor clearColor];
                lable.backgroundColor = [UIColor clearColor];
                imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemBgIphoneImage]];
                imggg.image = [[UIImage imageWithContentsOfFile:imgpath]stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0];
                imgArrowpath =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemArrowImage]];
                if (indexPath.row == 0 || indexPath.row == [arrmenuItems count]-2)
                {
                    
                    
                    int w = cell.frame.size.width ;// imggg.image.size.width;
                    int h = cell.frame.size.height ; //imggg.image.size.height;
                    
                    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
                    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
                    
                    CGContextBeginPath(context);
                    CGRect rect = CGRectMake(0, 0, w, h);
                    addRoundedRectToPath(context, rect, 12,15, indexPath.row == 0 ? 1 : 2 );
                    CGContextClosePath(context);
                    CGContextClip(context);
                    
                    CGContextDrawImage(context, rect, imggg.image.CGImage);
                    
                    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
                    CGContextRelease(context);
                    CGColorSpaceRelease(colorSpace);
                    
                   imggg.image =  [UIImage imageWithCGImage:imageMasked];
                    CGImageRelease(imageMasked);


                }
                cell.backgroundView = imggg;
                
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                CGRect frame = CGRectMake(0.0, 0.0, 14, 14);
                button.frame = frame;
                
                [button setBackgroundImage:[UIImage imageWithContentsOfFile:imgArrowpath] forState:UIControlStateNormal];
                button.backgroundColor = [UIColor clearColor];
                cell.accessoryView = button;
                
                cell.backgroundView.alpha = menuItemOpacity;
            }
            cell.clipsToBounds = YES;
        }
        else
        {
            
            if([menuItemBgIpadImage isEqualToString:@""]) //Use bgColor
                {
                    //UIColor *bgColor = [self colorForHex:menuItemBgColor];
                    UIColor *bgColor = [self colorForHex:menuItemBgColor:menuItemOpacity];
                    NSLog(@"bgColor %@",bgColor);
                    imggg.backgroundColor = bgColor ;//[UIColor clearColor];
                    NSLog(@"%d",indexPath.row);
                    NSLog(@"%f",cell.frame.size.width);
                    cell.backgroundColor = bgColor;
                    lable.backgroundColor = [UIColor clearColor];

                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    CGRect frame = CGRectMake(0.0, 0.0, 14, 14);
                    button.frame = frame;
                    imgArrowpath =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemArrowImage]];
                    [button setBackgroundImage:[UIImage imageWithContentsOfFile:imgArrowpath] forState:UIControlStateNormal];
                    button.backgroundColor = [UIColor clearColor];
                    cell.accessoryView = button;
                    
                }
                else
                {
                    cell.backgroundColor = [UIColor clearColor];
                    imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemBgIpadImage]];
                    imggg.frame = CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
//                    imggg.image = [UIImage imageWithContentsOfFile:imgpath];
                    UIImage *IMG = [self resizeImage:[UIImage imageWithContentsOfFile:imgpath] newSize:CGSizeMake(cell.frame.size.width, cell.frame.size.height)];
                    NSLog(@"%f",IMG.size.width);
                    imggg.image =  IMG;
                    NSLog(@"%f %f",imggg.frame.size.width,cell.frame.size.width);
                    imgArrowpath =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], menuItemArrowImage]];
                    if (indexPath.row == 0 || indexPath.row == [arrmenuItems count]-2)
                    {
                        int w = cell.frame.size.width ;// imggg.image.size.width;
                        int h = cell.frame.size.height ; //imggg.image.size.height;
                        
                        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
                        CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGImageAlphaPremultipliedFirst);
                        
                        CGContextBeginPath(context);
                        CGRect rect = CGRectMake(0, 0, w, h);
                        addRoundedRectToPath(context, rect, 12,15, indexPath.row == 0 ? 1 : 2 );
                        CGContextClosePath(context);
                        CGContextClip(context);
                        
                        CGContextDrawImage(context, rect, imggg.image.CGImage);
                        
                        CGImageRef imageMasked = CGBitmapContextCreateImage(context);
                        CGContextRelease(context);
                        CGColorSpaceRelease(colorSpace);
                        
                        imggg.image =  [UIImage imageWithCGImage:imageMasked];
                        CGImageRelease(imageMasked);
                        
                        
                    }
                    cell.backgroundView = imggg;
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    CGRect frame = CGRectMake(0.0, 0.0, 14, 14);
                    button.frame = frame;
                    
                    [button setBackgroundImage:[UIImage imageWithContentsOfFile:imgArrowpath] forState:UIControlStateNormal];
                    button.backgroundColor = [UIColor clearColor];
                    cell.accessoryView = button;
                    cell.backgroundView.alpha = menuItemOpacity;
                    
                }
            cell.clipsToBounds = YES;
        
        }

        
        
        NSLog(@"%d : %@",indexPath.row,menuItemName);
        if(menuItemName == (NSString *)[NSNull null])
        {
            menuItemName = @"";
        }
        lable.text = [NSString stringWithFormat:@"  %@",menuItemName];
        if (![Fontsize isEqualToString:@""] && ![FontStyle isEqualToString:@""]) {
            lable.font = [UIFont fontWithName:FontStyle size:[Fontsize floatValue]];
            lable.textColor = [self colorForHex:Textcolor];
            if([FontStyleType hasSuffix:@"Underline"])
            {
                lable.underline = true;
            }
        }
    }
    return cell;
}

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth,float ovalHeight, int roundedCornerPosition)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    
    if (roundedCornerPosition == 1)
    {
        CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
        CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
        CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 0);
        CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 0);
    }
    else
    {
        CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 0);
        CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 0);
        CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
        CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);

    }
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    isCalledFromBasiciPad = false;
    if(tableView == table) //more tabs list
    {
        isCalledFromMenu = false;
        lastTabTypeId = 0;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:(indexPath.row+4)]intValue] :indexPath.row+4];
        }
        else 
        {
            [self createURLAndLoad:[[arrVisibleTabs objectAtIndex:(indexPath.row+9)]intValue] :indexPath.row+9];
        }
    }
    else if(tableView == basicMenuTable)
    {
        [self processMenuItem:indexPath.row + 1];
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowH = 44.0;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && tableView !=basicMenuTable )
    {
        rowH = 66;
    }
    return rowH;
}

- (void)viewDidUnload
{
    [self setSquareView:nil];
    [self setPageDots:nil];
    
    nextPreviousControl = nil;
    keyboardToolbar =nil;
    _ScrollView = nil;
    prog = nil;
    _SquareView =  nil;
    _PageDots = nil;
    carousel1 = nil;
    arrpaths = nil;
    locationManager = nil;
    currentLocation = nil;
    table = nil;
    Webview = nil;
    dict = nil;
    oldTabs = nil;
    nTabs = nil;
    currentTabs = nil;
    dictSquareMenuSelectedItems = nil;
    dictTabMenuItemsArray = nil;
    dictCameraForm = nil;
    arr = nil;
    arrCameraFormFields = nil;
    arrFormFieldObjects = nil;
    appname = nil;
    TAB_TYPES = nil;
    arrapptabtypeid = nil;
    arrtabs = nil;
    arrtabnames = nil;
    arrtabicons = nil;
    arrtabimgs = nil;
    arrtabid = nil;
    arrapptabid = nil;
    arrtabhtml = nil;
    arrhtmlpaths = nil;
    arrurls = nil;
    arrtaburls = nil;
    arrpublisheddate = nil;
    arrdates = nil;
    arrHeaderBackgroungcolor = nil;
    arrHeaderBackgroungcoloripad = nil;
    arrheader = nil;
    arrheaderipad = nil;
    arrButtonBackgroundColor = nil;
    arrButtonBackgroundColoripad = nil;
    arrButtonBackgroungdImage = nil;
    arrHeaderBackgroungdImage = nil;
    arrButtonBackgroungdImageipad = nil;
    arrHeaderBackgroungdImageipad = nil;
    arrHeaderFontStyle = nil;
    arrbuttonFontStyle = nil;
    arrheaderfonttype = nil;
    arrHeaderTextcolor = nil;
    arrButtonTextcolor = nil;
    arrpassword = nil;
    arrpasswordstatus = nil;
    arrheaderfontsize = nil;
    arrbuttonfontsize = nil;
    arrtabsize = nil;
    arrtabhide = nil;
    arrVisibleTabs = nil;
    arrMenuTabsNavigated = nil;

    
    [locationManager stopUpdatingLocation];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    //stop locationManager
   
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    if(!isMainViewLoaded)
    {
        isMainViewLoaded = true;
        [super viewDidAppear:animated];
        [self checkstatus];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

-(BOOL)shouldAutorotate{
    
    return YES;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if (interfaceOrientation== UIInterfaceOrientationPortrait) 
        {
            return YES;
        }
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) 
    {
        if (interfaceOrientation == UIInterfaceOrientationLandscapeRight || interfaceOrientation == UIInterfaceOrientationLandscapeLeft )
        {
            return YES;
        }
    }
    return NO;
}

-(void)processMenuItem:(int)menuItemIndex
{
    isCalledFromMenu = true;
    strUrlForWebView = @"";
    [self.view bringSubviewToFront:tabbar];
    detailWebViewURLHistoryCount = 0;
    
    NSMutableDictionary *menuItem = [arrmenuItems objectAtIndex:menuItemIndex];
    
    bool isLinked = [[self getValueFromDictionary:menuItem :@"IsLink"] boolValue];
    
    if(!isLinked) //TODO: To remove negation of IsLinked variable when fixed from server side
    {
        
        NSString *strMsg = [NSString stringWithFormat:@"clicked on %d",menuItemIndex];
        
        NSString *MenuExistingAppTabId = [self getValueFromDictionary:menuItem :@"MenuExistingAppTabId"];
        NSString *MenuExternalLink = [self getValueFromDictionary:menuItem :@"MenuExternalLink"];
        NSString *MenuMailTo = [self getValueFromDictionary:menuItem :@"MenuMailTo"];
        NSString *MenuPhone = [self getValueFromDictionary:menuItem :@"MenuPhone"];
        
        int tabIdLinked = [MenuExistingAppTabId intValue];
        if(tabIdLinked > 0 )
        {
            strMsg = [NSString stringWithFormat:@"Load existing tab with Id %@",MenuExistingAppTabId];
            int strid, visibleTabIndex;
            for (int i=0; i<arrapptabtypeid.count; i++) {
                if ([MenuExistingAppTabId intValue]==[[arrtabid objectAtIndex:i] intValue]) {
                    strid = i;
                    break;
                }
            }
            
            NSLog(@"%d : %@",strid,strMsg);
            
            
            //Show back button when switching to other tab
            [backitem setHidden:NO];
            [backitem setEnabled:YES];
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
            {
                [self createURLAndLoad:strid :111];
            }
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
            {
                int itemTagForBasicMenu = 555;
                if([[arrapptabtypeid objectAtIndex:z] intValue] == 13 )
                {
                    itemTagForBasicMenu = 111;
                }
                [self createURLAndLoad:strid :itemTagForBasicMenu];
            }
        }
        else if(![MenuExternalLink isEqualToString:@""])
        {
            
            //strMsg = [NSString stringWithFormat:@"http://%@",MenuExternalLink];
            strMsg = MenuExternalLink;
            NSLog(@"%d : %@",menuItemIndex,strMsg);
            
            strUrlForWebView = strMsg;
            fullScreenURL = strMsg;
            
            NSURLRequest * request1 = [NSURLRequest requestWithURL: [NSURL URLWithString:strMsg]
                                                       cachePolicy: NSURLRequestUseProtocolCachePolicy
                                                   timeoutInterval: 1000];
            
           loader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad && isBasicMenu )
            {
                if (isCalledFromBasiciPad == true)
                {
                    DetailWebView.frame = CGRectMake(0, 0, 1024, 728);
                    [DetailWebView loadRequest:request1];
                    loader.frame = CGRectMake(500, 312, 37, 37);
                    IMGSEPERATOR.hidden = true;
                    [self.view bringSubviewToFront:DetailWebView];
                    tabbar.hidden = true;
                    acc = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    
                    acc.color = [UIColor blackColor];
                    
                    acc.center = DetailWebView.center;
                    [acc startAnimating];
//                  [self.view addSubview:acc];
                    [self.view bringSubviewToFront:acc];
                    [loader removeFromSuperview];
                    
                }
                else
                {
                    [DetailWebView loadRequest:request1];
                    loader.frame = CGRectMake(620, 350, 37, 37);
                    tabbar.hidden = false;
                    [self.view bringSubviewToFront:DetailWebView];
                    acc = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                    
                    acc.color = [UIColor blackColor];
                    
                    acc.center = DetailWebView.center;
                    [acc startAnimating];
                    [self.view addSubview:acc];
                    [self.view bringSubviewToFront:acc];
                    [loader removeFromSuperview];
                    loader = nil;
                    
                }
                
            }
            else
            {
                [Webview removeFromSuperview];
                [Webview release];
                
                lastTabIndex = z;
                lastTabTypeId = [[arrapptabtypeid objectAtIndex:z] intValue];
                [self storeLastTabIdInArr];
                
                [self initializeWebView];
                [Webview loadRequest:request1];
                if(isIPhone)
                {
                    loader.frame = CGRectMake(130, 180, 50, 50);
                }
                else
                {
                    loader.frame = CGRectMake(500, 312, 37, 37);
                }
            }

            loader.color = [UIColor blackColor];
            
            [loader startAnimating];
            [self.view addSubview:loader];
            [self.view bringSubviewToFront:barimg];
            [self.view bringSubviewToFront:backitem];
            [self.view sendSubviewToBack:_SquareView];
            barimg.hidden = false;
            [self.view bringSubviewToFront:loader];
            
        }

        else if(![MenuMailTo isEqualToString:@""])
        {
            strMsg = [NSString stringWithFormat:@"mailto:%@",MenuMailTo];
            NSLog(@"%d : %@",menuItemIndex,strMsg);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strMsg]];
        }
        else if(![MenuPhone isEqualToString:@""])
        {
            doLaunchDialer = true;
            telURL = [NSString stringWithFormat:@"tel:%@",MenuPhone];
            UIAlertView *alt = [[[UIAlertView alloc]initWithTitle:@"" message:MenuPhone delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles: @"Ring", nil]autorelease];
            
            [alt show];
        }
        
    }
    //[MenuView bringSubviewToFront:DetailWebView];
    //[MenuView bringSubviewToFront:loader];
}


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
        if(arrmenuItems != nil)
        {
            return [arrmenuItems count]-1;
        }
        else
        {
            return 0;
        }
    
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
    //limit the number of items views loaded concurrently (for performance reasons)
    return [arrmenuItems count]-1;
}
- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
	//note: placeholder views are only displayed on some carousels if wrapping is disabled
	return 0;
}
- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    if(index+1 >= [arrmenuItems count])
    {
        return view;
    }
    NSMutableDictionary *menuItemDict = [arrmenuItems objectAtIndex:index + 1 ];
    NSString* MenuIpadImage = [self getValueFromDictionary:menuItemDict:@"MenuIpadImage"];
    NSString* MenuIphoneImage = [self getValueFromDictionary:menuItemDict:@"MenuIphoneImage"];
    
    NSString *imgpath;
    NSString *ImagedataPath = [self getTabFolderBasePath];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {

        imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_2_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], MenuIphoneImage]];
        
        NSLog(@"Menu Image :%@",imgpath);
        
       UIImage *imggg = [[UIImage imageWithContentsOfFile:imgpath]stretchableImageWithLeftCapWidth:0.0 topCapHeight:0.0];

        view = [[[UIImageView alloc] initWithImage:imggg]autorelease];
        view.frame = CGRectMake(0, 0, 145,272.0f);

        view.layer.masksToBounds = NO;
    }
    else
    {
        
        imgpath  =[ImagedataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_3_%d/Images/%@",appid,[[arrapptabid objectAtIndex:z] intValue], MenuIpadImage]];
        
        NSLog(@"Menu Image :%@",imgpath);
        
        UIImage *imggg = [[UIImage imageWithContentsOfFile:imgpath]stretchableImageWithLeftCapWidth:0.0 topCapHeight:2.0];
        view = [[[UIImageView alloc] initWithImage:imggg]autorelease];
        view.frame=CGRectMake(0, 0, 224.0f,450.0f);
        //view.layer.cornerRadius = 10.0f;
        view.layer.masksToBounds = NO;

    }
    return view;
}


- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
          
        default:
        {
            return value;
        }
    }
}

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel
{
    _PageDots.currentPage =carousel.currentItemIndex;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    NSInteger menuItemIndex = carousel1.currentItemIndex;
    [dictSquareMenuSelectedItems setObject:[NSNumber numberWithInt:menuItemIndex]forKey:[NSNumber numberWithInt:z]];
    [self processMenuItem:index+1];
}

-(void)setSelectedTab:(int)index
{
    Boolean isVisibleTab = false;
    int visibleTabIndex = 0;
    
    for (int i=0; i<arrVisibleTabs.count; i++) {
        if (index == [[arrVisibleTabs objectAtIndex:i] intValue]) {
            isVisibleTab = true;
            visibleTabIndex = i;
            break;
        }
    }
    
    if(isVisibleTab)
    {
        // Return YES for supported orientations
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
        {
            switch (visibleTabIndex) {
                case 0:
                    [tabbar setSelectedItem:tab1];
                    
                    break;
                    
                case 1:
                    [tabbar setSelectedItem:tab2];
                    
                    break;
                    
                case 2:
                    [tabbar setSelectedItem:tab3];
                    
                    break;
                case 3:
                    [tabbar setSelectedItem:tab4];
                    
                    break;
                case 4:
                    if([arrtabid count] == 5){
                        [tabbar setSelectedItem:tab6];
                    }                            break;
                default:
                    break;
            }/**/
            
        }
        else if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            switch (visibleTabIndex) {
                case 0:
                    [tabbar setSelectedItem:tab1];
                    
                    break;
                    
                case 1:
                    [tabbar setSelectedItem:tab2];
                    
                    break;
                    
                case 2:
                    [tabbar setSelectedItem:tab3];
                    
                    break;
                case 3:
                    [tabbar setSelectedItem:tab4];
                    
                    break;
                case 4:
                    [tabbar setSelectedItem:tab5];
                    
                    break;
                case 5:
                    [tabbar setSelectedItem:tab6];
                    
                    break;
                case 6:
                    [tabbar setSelectedItem:tab7];
                    
                    break;
                case 7:
                    [tabbar setSelectedItem:tab8];
                    
                case 8:
                    if([arrtabid count] == 9){
                        [tabbar setSelectedItem:tab9];
                    }
                    break;
                default:
                    break;
            }/**/
        }
    }

}

-(CGFloat)CheckDeviceSize
{
    CGSize result ;
    CGFloat resi;
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
     result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
        // iPhone Classic
            resi = 0.0f;
        }
        if(result.height == 568)
        {
        // iPhone 5
            resi = 88;
        }
//    }
    return resi;
}

- (void)initializeWebView
{
    CGRect ScreenSize = [[UIScreen mainScreen ]bounds];
    float heightFactor = 0, screenHeight = ScreenSize.size.height;
    float hFactorForFullscreen = 0;
    
    if(isCalledFromBasiciPad)
    {
        hFactorForFullscreen = 49;
    }
    strUDID = [BPXLUUIDHandler UUID];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if( screenHeight > 480.0)
        {
            heightFactor = screenHeight - 480;
        }
        Webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, 320,367+heightFactor)];
        barimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(69, 11, 173, 21)];//367458
        table= nil;
    }
    else
    {
        Webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, 44, 1024, 655+hFactorForFullscreen)];
        barimg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 1024, 44)];
        lbl = [[UILabel alloc]initWithFrame:CGRectMake(430, 11, 173, 21)];//367458
        table= nil;
    }
    
    [self.view addSubview:Webview];
    Webview.delegate = self;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
    {
        Webview.scrollView.bounces = NO;
    }
    else
    {
        [[Webview.subviews lastObject] setBounces:NO];
    }
     [Webview canGoBack];
     Webview.dataDetectorTypes = UIDataDetectorTypeLink | UIDataDetectorTypePhoneNumber;
     [Webview canGoForward];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    CGPoint p = [gestureRecognizer locationInView:basicMenuTable];
    
    NSIndexPath *indexPath = [basicMenuTable indexPathForRowAtPoint:p];
//    if (indexPath == nil)
//        NSLog(@"long press on table view but not on a row");
//    else
//        NSLog(@"long press on table view at row %d", indexPath.row);
    if(!isCalledFromBasiciPad)
    {
        isCalledFromBasiciPad = true;
        [self processMenuItem:indexPath.row + 1];
    }
}

-(void)showBlackViewLoader
{
    [blackview removeFromSuperview];
    [prog removeFromSuperview];
    [lblpercent removeFromSuperview];
    float labelY = blackview.frame.origin.y-45-heightFactor;
    labelY -= heightFactor == 0 ? 40 : 0;
    
    loadLabel.frame = CGRectMake(-0.5, labelY, 200, 28);
    loadLabel.text = @"Meddelandet skickas";
    [self.view addSubview:blackview];
    [self.view bringSubviewToFront:blackview];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    camFormSelectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [camFormImageView setImage:camFormSelectedImage];
    
    [self dismissModalViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark UIResponder Override
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touchesBegan:withEvent:");
    
    /*--
     * Override UIResponder touchesBegan:withEvent: to resign the textView when the user taps the background
     * Use fast enumeration to go through the subview property of UIView
     * Any object that is the current first repsonder will resign that status
     * Make a call to super to take care of any unknown behavior that touchesBegan:withEvent: needs to do behind the scenes
     --*/
    
    for (UITextView *textView in self.view.subviews) {
        if ([textView isFirstResponder])
        {
            [textView resignFirstResponder];
        }
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //[self animateTextField: textField up: YES];
    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    
    [self moveFieldsUp:textFieldRect];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self animateTextField: textField up: NO];
    [self moveFieldsDown];
}

- (void) moveFieldsUp:(CGRect)textFieldRect
{
    CGRect viewRect = [cameraFormView.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        //heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    
    CGRect viewFrame = cameraFormView.frame;
    viewFrame.origin.y -= animatedDistance;
    
//    CGRect keyboardToolbarFrame = keyboardToolbar.frame;
//    keyboardToolbarFrame.origin.y += animatedDistance;
//    keyboardToolbar.frame = keyboardToolbarFrame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [cameraFormView setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (void)moveFieldsDown
{
    CGRect viewFrame = cameraFormView.frame;
    viewFrame.origin.y += animatedDistance;
    
//    CGRect keyboardToolbarFrame = keyboardToolbar.frame;
//    keyboardToolbarFrame.origin.y -= animatedDistance;
//    keyboardToolbar.frame = keyboardToolbarFrame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [cameraFormView setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement, kbHeight = 150 ;//+ heightFactor;
    CGRect frame = [textField convertRect:textField.frame toView:self.view];
    if(up)
    {
        keyboardOffset = 0 ;
        if(frame.origin.y > kbHeight)
        {
            keyboardOffset = frame.origin.y - cameraFormView.frame.origin.y - kbHeight  - _ScrollView.contentOffset.y;
        }
        movement = -keyboardOffset;
        
        movement = movement < 0 ? movement : 0;
    }
    else
    {
        movement = keyboardOffset;
    }

    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    cameraFormView.frame = CGRectOffset(cameraFormView.frame, 0, movement);
    [UIView commitAnimations];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //[self animateTextView: textView up: YES];
    
    CGRect textFieldRect = [self.view.window convertRect:textView.bounds fromView:textView];
    
    [self moveFieldsUp:textFieldRect];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    //[self animateTextView: textView up: NO];
    
    [self moveFieldsDown];
}

- (void) animateTextView: (UITextView*) textField up: (BOOL) up
{
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement, kbHeight = 150; // + heightFactor;
    CGRect frame = [textField convertRect:textField.frame toView:self.view];
    if(up)
    {
        keyboardOffset = 0 ;
        if(frame.origin.y > kbHeight)
        {
            keyboardOffset = frame.origin.y - kbHeight ; //- _ScrollView.contentOffset.y;
        }
        movement = -keyboardOffset;
        
        movement = movement < 0 ? movement : 0;
    }
    else
    {
        movement = keyboardOffset;
    }
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    cameraFormView.frame = CGRectOffset(cameraFormView.frame, 0, movement);
    [UIView commitAnimations];
}

- (IBAction)dismissKeyboard:(id)sender
{
	[[self.view findFirstResponder] resignFirstResponder];
}

- (IBAction)nextPrevious:(id)sender
{
	UIView *responder = [self.view findFirstResponder];
	
    int loop;
    
	switch([(UISegmentedControl *)sender selectedSegmentIndex]) {
		case 0:
            // previous
            for( loop = 0 ; loop < [arrFormFieldObjects count] ; loop++)
            {
                if(responder == [arrFormFieldObjects objectAtIndex:loop])
                {
                    break;
                }
            }

            if(loop > 0)
            {
                loop--;
            }
            else
            {
                loop = [arrFormFieldObjects count]-1;
            }
			break;
		case 1:
			// next
            for( loop = 0 ; loop < [arrFormFieldObjects count] ; loop++)
            {
                if(responder == [arrFormFieldObjects objectAtIndex:loop])
                {
                    break;
                }
            }
            
            if(loop < [arrFormFieldObjects count]-1)
            {
                loop++;
            }
            else
            {
                loop = 0;
            }
			break;
	}
    
    if([[arrFormFieldObjects objectAtIndex:loop] isKindOfClass:[UITextView class]])
    {
        UITextView *tv = (UITextView*)[arrFormFieldObjects objectAtIndex:loop] ;
        [tv becomeFirstResponder];
    }
    else
    {
        UITextField *tf = (UITextField*)[arrFormFieldObjects objectAtIndex:loop] ;
        [tf becomeFirstResponder];
    }
    
}

#pragma mark -
#pragma mark UIWindow Keyboard Notifications

- (void)keyboardWillShow:(NSNotification *)notification
{
	CGPoint beginCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterBeginUserInfoKey] CGPointValue];
	CGPoint endCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterEndUserInfoKey] CGPointValue];
	CGRect keyboardBounds = [[[notification userInfo] valueForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
	UIViewAnimationCurve animationCurve	= [[[notification userInfo] valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
	NSTimeInterval animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	if (nil == keyboardToolbar) {
		
		if(nil == keyboardToolbar) {
			keyboardToolbar = [[[UIToolbar alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width,44)]autorelease];
			keyboardToolbar.barStyle = UIBarStyleBlackTranslucent;
			keyboardToolbar.tintColor = [UIColor darkGrayColor];
			
			UIBarButtonItem *barButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissKeyboard:)]autorelease];
			UIBarButtonItem *flex = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]autorelease];
			
			UISegmentedControl *control = [[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:
																					 NSLocalizedString(@"Previous",@"Previous form field"),
																					 NSLocalizedString(@"Next",@"Next form field"),
																					 nil]]autorelease];
			control.segmentedControlStyle = UISegmentedControlStyleBar;
			control.tintColor = [UIColor darkGrayColor];
			control.momentary = YES;
			[control addTarget:self action:@selector(nextPrevious:) forControlEvents:UIControlEventValueChanged];
			
			UIBarButtonItem *controlItem = [[[UIBarButtonItem alloc] initWithCustomView:control]autorelease];
			
			self.nextPreviousControl = control;
			
			
			NSArray *items = [[[NSArray alloc] initWithObjects:controlItem, flex, barButtonItem, nil]autorelease];
			[keyboardToolbar setItems:items];
//			[control release];
//			[barButtonItem release];
//			[flex release];
//			[items release];
			
			keyboardToolbar.frame = CGRectMake(beginCentre.x - (keyboardBounds.size.width/2),
											   beginCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height,
											   keyboardToolbar.frame.size.width,
											   keyboardToolbar.frame.size.height);
			
			[self.view addSubview:keyboardToolbar];
		}
	}
	
	[UIView beginAnimations:@"RS_showKeyboardAnimation" context:nil];
	[UIView setAnimationCurve:animationCurve];
	[UIView setAnimationDuration:animationDuration];
	
	keyboardToolbar.alpha = 1.0;
	keyboardToolbar.frame = CGRectMake(endCentre.x - (keyboardBounds.size.width/2),
									   endCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height - self.view.frame.origin.y,
									   keyboardToolbar.frame.size.width,
									   keyboardToolbar.frame.size.height);

	[self.view bringSubviewToFront:keyboardToolbar];
	
    [UIView commitAnimations];
	
	keyboardToolbarShouldHide = YES;
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
	if (nil == keyboardToolbar || !keyboardToolbarShouldHide) {
		return;
	}
	
	//	CGPoint beginCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterBeginUserInfoKey] CGPointValue];
	CGPoint endCentre = [[[notification userInfo] valueForKey:UIKeyboardCenterEndUserInfoKey] CGPointValue];
	CGRect keyboardBounds = [[[notification userInfo] valueForKey:UIKeyboardBoundsUserInfoKey] CGRectValue];
	UIViewAnimationCurve animationCurve	= [[[notification userInfo] valueForKey:UIKeyboardAnimationCurveUserInfoKey] intValue];
	NSTimeInterval animationDuration = [[[notification userInfo] valueForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
	
	[UIView beginAnimations:@"RS_hideKeyboardAnimation" context:nil];
	[UIView setAnimationCurve:animationCurve];
	[UIView setAnimationDuration:animationDuration];
	
	keyboardToolbar.alpha = 0.0;
	keyboardToolbar.frame = CGRectMake(endCentre.x - (keyboardBounds.size.width/2),
									   endCentre.y - (keyboardBounds.size.height/2) - keyboardToolbar.frame.size.height,
									   keyboardToolbar.frame.size.width,
									   keyboardToolbar.frame.size.height);
	[UIView commitAnimations];
    
//    
//    [UIView beginAnimations: @"anim" context: nil];
//    [UIView setAnimationBeginsFromCurrentState: YES];
//    [UIView setAnimationDuration: 1.0f];
//    cameraFormView.frame = CGRectOffset(cameraFormView.frame, 0, keyboardOffset);
//    [UIView commitAnimations];
    

}

-(void)dealloc
{
    
    [nextPreviousControl release];
    [keyboardToolbar release];
    [_ScrollView release];
    [prog release];
    [_SquareView release];
    [_PageDots release];
    [carousel1 release];
    [arrpaths release];
    [locationManager release];
    [currentLocation release];
    [table release];
    [Webview release];
    [dict release];
    [oldTabs release];
    [nTabs release];
    [currentTabs release];
    [dictSquareMenuSelectedItems release];
    [dictTabMenuItemsArray release];
    [dictCameraForm release];
    [arr release];
    [arrCameraFormFields release];
    [arrFormFieldObjects release];
    [appname release];
    [TAB_TYPES release];
    [arrapptabtypeid release];
    [arraytags release];
    [arrtabs release];
    [arrtabnames release];
    [arrtabicons release];
    [arrtabimgs release];
    [arrtabid release];
    [arrapptabid release];
    [arrtabhtml release];
    [arrhtmlpaths release];
    [arrurls release];
    [arrtaburls release];
    [arrpublisheddate release];
    [arrdates release];
    [arrHeaderBackgroungcolor release];
    [arrHeaderBackgroungcoloripad release];
    [arrheader release];
    [arrheaderipad release];
    [arrButtonBackgroundColor release];
    [arrButtonBackgroundColoripad release];
    [arrButtonBackgroungdImage release];
    [arrHeaderBackgroungdImage release];
    [arrButtonBackgroungdImageipad release];
    [arrHeaderBackgroungdImageipad release];
    [arrHeaderFontStyle release];
    [arrbuttonFontStyle release];
    [arrheaderfonttype release];
    [arrHeaderTextcolor release];
    [arrButtonTextcolor release];
    [arrpassword release];
    [arrpasswordstatus release];
    [arrheaderfontsize release];
    [arrbuttonfontsize release];
    [arrtabsize release];
    [arrtabhide release];
    [arrVisibleTabs release];
    [arrMenuTabsNavigated release];
    [MenuView release];
    [DoubleTap release];
    [blackview release];
    [loadLabel release];
    [loading release];
    [we release];
    [ARRIMAGES release];
    [ARRNAMES release];
    [tab6 release];
    [tab1 release];
    [tab2 release];
    [tab3 release];
    [tab4 release];
    [tab5 release];
    [tab7 release];
    [tab8 release];
    [tab9 release];
    [tab10 release];
    [acc release];
    [actview release];
    [basicMenuTable release];
    [webtop release];
    [webbottom release];
    [IMGSEPERATOR release];
    [DetailWebView release];
    [Basicimgbackground release];
    [WebTopSquare release];
    [cameraFormView release];
    [cameraFormWebView release];
    [camFormImageView release];
    [lblpercent release];
    [confirmationMessageWebView release];
    [confirmationMessageView release];
    [super dealloc];
    
}

@end
