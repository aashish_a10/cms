//
//  UITextFieldExt.m
//  CMSAPPSALES
//
//  Created by Mac User on 12/06/13.
//  Copyright (c) 2013 vikram@in.eclatinfotech.com. All rights reserved.
//

#import "UITextFieldExt.h"

@implementation UITextFieldExt

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 7 , 7 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 7 , 7 );
}
@end
