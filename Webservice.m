//
//  Webservice.m
//  CMSApp
//
//  Created by Mac User on 08/04/12.
//  Copyright (c) 2012. All rights reserved.
//

#import "Webservice.h"
#import "ViewController.h"
#import "AppDelegate.h"
@implementation Webservice


-(NSString *) callWebService:(int)uid :(int)appid :(NSString*)uname :(NSString*)upwd:(NSString*) serverURL
{
    NSString *strRequestString = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                  @"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                  @"<soap:Body>"
                                  @"<GetApplicationTabCredentailsStatus xmlns=\"http://tempuri.org/\">"
                                  @"<userID>%d</userID>"
                                  @"<appTabID>%d</appTabID>"
                                  @"<userName>%@</userName>"
                                  @"<password>%@</password>"
                                  @"</GetApplicationTabCredentailsStatus>"
                                  @"</soap:Body>"
                                  @"</soap:Envelope>",uid,appid,uname,upwd];
    
	NSString *strRequestLength = [NSString stringWithFormat:@"%d",[strRequestString length]];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@services/GetCredentials.asmx?op=GetApplicationTabCredentailsStatus",serverURL];
	NSURL *url = [NSURL URLWithString:urlStr];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
	
	// setting header fields
	[request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:strRequestLength forHTTPHeaderField:@"Content-Length"];
	[request addValue:@"http://tempuri.org/GetApplicationTabCredentailsStatus" forHTTPHeaderField:@"SOAPAction"];
	//setting body fields
	NSData *requestData = [strRequestString dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:requestData];
	[request setHTTPMethod:@"POST"];
	
	//asynchronous
    NSURLConnection *connection1 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    NSLog(@"tempStr%@,%@",tempStr,connection1);
    return tempStr;
}


-(BOOL)callWebService123:(int)appid :(NSString*)udid :(NSString*)dtoken:(NSString*) serverURL
{
    NSString *strRequestString = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><SaveApplicationDeviceInfo xmlns=\"http://tempuri.org/\"><AppID>%d</AppID><IMEI>%@</IMEI><RegistrationID>%@</RegistrationID><PlateformID>2</PlateformID></SaveApplicationDeviceInfo></soap:Body></soap:Envelope>",appid,udid,dtoken];
    
	NSString *strRequestLength = [NSString stringWithFormat:@"%d",[strRequestString length]];
	
    NSString *urlStr = [NSString stringWithFormat:@"%@services/sendappregistration.asmx?op=SaveApplicationDeviceInfo",serverURL];
	NSURL *url = [NSURL URLWithString:urlStr];
    
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
	
	// setting header fields
    
	[request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:strRequestLength forHTTPHeaderField:@"Content-Length"];
	[request addValue:@"http://tempuri.org/SaveApplicationDeviceInfo" forHTTPHeaderField:@"SOAPAction"];
	
	//setting body fields
	NSData *requestData = [strRequestString dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:requestData];
	[request setHTTPMethod:@"POST"];
	
    connection123 = [[NSURLConnection alloc]initWithRequest:request delegate:self];
	return aa;
}


-(NSString*) callWebServiceapp:(int)uid :(int)appid :(NSString*)uname :(NSString*)upwd:(NSString*) serverURL
{
    NSString *strRequestString = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>"
                                  @"<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">"
                                  @"<soap:Body>"
                                  @"<GetApplicationCredentailsStatus xmlns=\"http://tempuri.org/\">"
                                  @"<userID>%d</userID>"
                                  @"<appID>%d</appID>"
                                  @"<userName>%@</userName>"
                                  @"<password>%@</password>"
                                  @"</GetApplicationCredentailsStatus>"
                                  @"</soap:Body>"
                                  @"</soap:Envelope>",uid,appid,uname,upwd];
    
	NSString *strRequestLength = [NSString stringWithFormat:@"%d",[strRequestString length]];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@services/GetCredentials.asmx?op=GetApplicationCredentailsStatus",serverURL];
	NSURL *url = [NSURL URLWithString:urlStr];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url];
	
	// setting header fields
	[request addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request addValue:strRequestLength forHTTPHeaderField:@"Content-Length"];
	[request addValue:@"http://tempuri.org/GetApplicationCredentailsStatus" forHTTPHeaderField:@"SOAPAction"];
	//setting body fields
	NSData *requestData = [strRequestString dataUsingEncoding:NSUTF8StringEncoding];
	[request setHTTPBody:requestData];
	[request setHTTPMethod:@"POST"];
	
	//asynchronous
    connectionapp = [[NSURLConnection alloc]initWithRequest:request delegate:self];
    NSLog(@"tempStr%@,%@",tempStr,connectionapp);
    return tempStr;
}

#pragma mark Webservice delegate methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	responseData = [[NSMutableData alloc]init];
	NSLog(@"didReceiveResponse");
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    
	[responseData appendData:data];
	NSLog(@"didReceiveData");
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
	NSLog(@"connectionDidFinishLoading");
    //	NSString *strResponse = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
	if (connection == connection123)
    {
        flag = 222;
    }

	//Xml parsing steps
	NSXMLParser *parser = [[NSXMLParser alloc]initWithData:responseData];
	parser.delegate = self;
	[parser parse];
	resu = tempStr;
    NSLog(@"resu  %@",resu);
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError %@",[error description]);
}

#pragma mark XmlParsing methods

// due to lack of power we r doing a static parsing

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
   attributes:(NSDictionary *)attributeDict
{
	NSLog(@"didStartElement %@",elementName);
    if (flag == 222)
    {
        if ([elementName isEqual:@"SaveApplicationDeviceInfoResult"])
        {
            
        }
    }
	if ([elementName isEqual:@"GetApplicationTabCredentailsStatusResult"])
    {
		
	}
    
    if ([elementName isEqual:@"GetApplicationCredentailsStatusResult"])
    {
		
	}
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	NSLog(@"foundCharacters %@",string);
    tempStr = string;
    resu = string;
    NSLog(@"foundCharacters  tempStr %@",tempStr);
    if (flag==222)
    {
        aa = [string boolValue];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
 namespaceURI:(NSString *)namespaceURI
qualifiedName:(NSString *)qName
{
	NSLog(@"didEndElement %@",elementName);
	if ([elementName isEqual:@"GetApplicationTabCredentailsStatusResult"])
    {
        resu = tempStr;
    }
    if ([elementName isEqual:@"GetApplicationCredentailsStatusResult"])
    {
        resu = tempStr;
    }
    
}

-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
	NSLog(@"parseErrorOccurred %@",[parseError description]);
}

@end
