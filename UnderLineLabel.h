#import <UIKit/UIKit.h>
/**
  * UnderLineLabel class
 **/

@interface UnderLineLabel:UILabel{
    BOOL underline;
    BOOL startFromZero;
}

// call setUnderline to YES/NO to show/hide the underline, this doesn't force a redraw
@property (nonatomic,getter=isUnderlined) BOOL underline;
@property (nonatomic,getter=shouldStartFromZero) BOOL startFromZero;

@end